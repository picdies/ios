//
//  FeedCollectionViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 11/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import MXParallaxHeader

extension FeedCollectionViewController {
	enum TableKeys:String, CaseIterable{
		case feed
		case article
		case info
		case publicity
		
		var cellName: String {
			get {
				switch self {
					case .feed: return "FeedCollectionBigCell"
					case .article: return "ArticleCollectionMediumCell"
					case .info: return "InfoCollectionBigCell"
					case .publicity: return "PublicityCollectionBigCell"
				}
			}
		}
		
		
		var heightForRowAt:CGSize {
			switch self {
				case .feed: return CGSize(width: (UIScreen.main.bounds.width / 2) - 1, height: ((UIScreen.main.bounds.width / 2) - 1) * 16/9)
				case .article: return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
				case .info: return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
				case .publicity: return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
			}
		}
		
		func registerIn(collectionView: UICollectionView) { collectionView.register(UINib(nibName: self.cellName, bundle: nil), forCellWithReuseIdentifier: self.cellName)}
		
	}
}


class FeedCollectionViewController:UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, APIFetcherDelegate, FeedCollectionBigCellProtocol, ArticleCollectionMediumProtocol {
	
	@IBOutlet weak var picdiesTitleHeaderTop: UILabel!
	@IBOutlet weak var headerViewHeight: NSLayoutConstraint!
	@IBOutlet weak var searchButton: UIButton!
	@IBOutlet weak var viewGrayTopHeader: UIView!
	@IBOutlet weak var buttonAddSearchBar: UIButton!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var headerView: UIView!
	
	var refreshController:UIRefreshControl?
	var feeds = [Feed]()

	// MARK: Life cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupRefreshController()
		self.registerNib()
		self.feeds = APIFetcher.shared.allCached()
		self.request()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.collectionView?.reloadData()
	}
	
	override func viewDidLayoutSubviews() {
		UIView.animate(withDuration: 0.2) {
			self.setNeedsStatusBarAppearanceUpdate()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
		self.refreshController?.endRefreshing()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(true)
	}
	
	override var preferredStatusBarStyle : UIStatusBarStyle {
		return .darkContent
	}
	
	override var prefersStatusBarHidden: Bool {
		return false
	}
	
	// MARK: Request
	
	@objc func request() {
		self.collectionView.activityIndicator(show: self.feeds.count == 0)
		APIFetcher.shared.request(.feeds, delegate: self) { (response) in
			self.responseFetchFeeds(response: response)
		}
	}
	
	func responseFetchFeeds(response : Error?) {
		if let _ = response {
			return
		} else {
			self.feeds = APIFetcher.shared.allCached()
			self.collectionView?.activityIndicator(show: self.feeds.count == 0)
			self.collectionView?.reloadData()
			DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
				self.refreshController?.endRefreshing()
			})
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseFetchFeeds(response: completion)
	}
	
	// MARK: Collection View
	
	func registerNib() {
		TableKeys.allCases.forEach { (key) in
			key.registerIn(collectionView: self.collectionView)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return min(max(self.feeds.count - section * 8, 0), 9)
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return Int(ceil(Float(self.feeds.count) / 8))
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if indexPath.row == 8 {
			if indexPath.section % 5 == 0 {
				return TableKeys.info.heightForRowAt
			}
			if indexPath.section % 3 == 0 {
				return TableKeys.article.heightForRowAt
			}
			return TableKeys.publicity.heightForRowAt
		}
		return TableKeys.feed.heightForRowAt
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		var id = TableKeys.feed.cellName
		if indexPath.row == 8 {
			if indexPath.section % 5 == 0 {
				id = TableKeys.info.cellName
			}
			if indexPath.section % 3 == 0 {
				id = TableKeys.article.cellName
			}
			id = TableKeys.publicity.cellName
		}
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)
		
		switch id {
			case TableKeys.feed.cellName:
				(cell as! FeedCollectionBigCell).configure(delegate: self, feed: self.feeds[(indexPath.row) + (indexPath.section * 8)])
			case TableKeys.article.cellName:
				(cell as! ArticleCollectionMediumCell).configure(delegate: self, article: Article(dict: [:]))
			case TableKeys.info.cellName:
				(cell as! InfoCollectionBigCell).configure()
			case TableKeys.publicity.cellName:
				if Ads.shared.nativeAds.count > 0 {
					let ad = Ads.shared.nativeAds[(indexPath.section % Ads.shared.nativeAds.count)]
					ad.rootViewController = self
					(cell as! PublicityCollectionBigCell).configure(nativeAd: ad)
				}
			default: break
		}
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch collectionView.cellForItem(at: indexPath)?.reuseIdentifier {
			case TableKeys.feed.cellName:
				let viewController = FeedViewController.instantiate(feed: self.feeds[(indexPath.row) + (indexPath.section * 8)], andIndex: 0, isPlaying: true) as! FeedViewController
				self.navigationController?.pushViewController(viewController, animated: true)
			case TableKeys.article.cellName:
				break
			case TableKeys.info.cellName:
				break
			case TableKeys.publicity.cellName:
				break
			default: break
		}
	}
	
	@IBAction func tapOpenPickerView(_ sender: Any) {
		if let b = sender as? UIButton { b.animateButton(color: nil, image: nil, withCompletion: {})}
		PickerView.shared.showPicker(with: self.navigationController!)
	}
	
	// MARK: Delegate
	
	func tapLike(cell: FeedCollectionBigCell) {
		var image = UIImage(named: "like/like-fill")
		var tint = UIColor.picdiesRed
		if UserStorage.likedFeed.contains(cell.feed!.id) {
			image = UIImage(named: "like/like")
			tint = .white
			UserStorage.likedFeed.removeAll(where: {$0 == cell.feed?.id})
			cell.feed?.interaction?.like! -= 1
			if let id = cell.feed?.interaction?.id {
				APIFetcher.shared.request(.updateFeedLike(id, [.like:-1]), delegate: self) { (response) in }
			}
		} else {
			Vibration.soft.vibrate()
			UserStorage.likedFeed.append(cell.feed!.id)
			cell.feed?.interaction?.like! += 1
			if let id = cell.feed?.interaction?.id {
				APIFetcher.shared.request(.updateFeedLike(id, [.like:1]), delegate: self) { (response) in }
			}
		}
		if let button = cell.like_button {
			button.animateButton(color : tint, image : image!) {
				self.collectionView?.reloadItems(at: [self.collectionView.indexPath(for: cell) ?? IndexPath()])
			}
		}
	}
	
	// MARK: Article delegate
	
	func tapBookmark(cell: ArticleCollectionMediumCell) { }
	
	// MARK: Add Pull to refresh
	
	func setupRefreshController() {
		self.refreshController = UIRefreshControl()
		self.refreshController?.addTarget(self, action: #selector(self.request), for: .touchUpInside)
		self.collectionView?.refreshControl = self.refreshController
	}
}
