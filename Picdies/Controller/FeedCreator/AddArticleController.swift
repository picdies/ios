//
//  AddArticleController.swift
//  Picdies
//
//  Created by Thomas Gallier on 24/02/2021.
//

import Foundation
import UIKit
import SDWebImage
import AVKit
import SwiftDate


class AddArticleController:UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, APIFetcherDelegate {
	
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var mainImage: UIImageView!
	@IBOutlet weak var btnBack: UIButton!
	@IBOutlet weak var viewAddArticle: UIView!
	
	var newFeed:NewFeed!
	var articles = [Article]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setup()
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.navigationController?.navigationBar.isHidden = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
		self.navigationController?.navigationBar.isHidden = false
	}
	
	// MARK: Request
	
	func returnCacheRequest(completion: Error?) {
		print("return cache")
	}
	
	// MARK: Setup
	
	func setup() {
		self.setupContent()
		self.setupGesture()
	}
	
	func setupGesture() {
		let tapAddArticle = UITapGestureRecognizer(target: self, action: #selector(self.tapAddArticle(_:)))
		tapAddArticle.numberOfTouchesRequired = 1
		tapAddArticle.numberOfTapsRequired = 1
		
		viewAddArticle.addGestureRecognizer(tapAddArticle)
	}
	
	// MARK: Setup Images scroll
	
	func setupContent() {
		DispatchQueue.main.async {
			switch self.newFeed?.feed_type {
				case .picture:
					self.mainImage.image = UIImage(data: (self.newFeed.new_ressources.first?.data)!)
				case .video:
					let r = self.newFeed?.new_ressources.first(where: { (ressource) -> Bool in
						return ressource.type == .picture
					})
					self.mainImage.image = UIImage(data: (r?.data)!)
				default: break
			}
		}
	}
	
	// MARK: Action
	
	@IBAction func backAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	
	@IBAction func tapNext(_ sender: Any) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let vc = storyboard.instantiateViewController(identifier: "AddInfoNewArticleController") as! AddInfoNewArticleController
		vc.newFeed = self.newFeed
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
	@objc func tapAddArticle(_ sender: UITapGestureRecognizer) {
		sender.view?.smallAnimateView()
	}
	
	
	
}
