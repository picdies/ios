//
//  FeedCreatorController.swift
//  Picdies
//
//  Created by Thomas Gallier on 16/02/2021.
//

import Foundation
import UIKit
import SDWebImage
import AVKit
import SwiftDate

extension AddInfoNewArticleController {
	enum InfoType:String, CaseIterable{
		case description
		case tags
		case users
	}
}

extension AddInfoNewArticleController {
	func getAllSearchBar() -> [UISearchBar] {
		return [ searchBarTags, searchBarUsers]
	}
}

extension AddInfoNewArticleController {
	func publishFeed() {
		let documentsFolderURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
		
		if self.newFeed.feed_type == .picture {
			
			var picturesURL = [URL]()
			self.newFeed.new_ressources.forEach { (ressource) in
				if ressource.type == .picture {
					let path = documentsFolderURL.appendingPathComponent(String.random() + ".png")
					try? ressource.data?.write(to: path)
					picturesURL.append(path)
				}
			}
			
			if picturesURL.count > 0 {
				let body:[APIRequest.APIKeysBody:Any] = [.name : self.newFeed.feed_name, .type: "picture", .user: AccountManager.shared.appUser?.id, .picture: picturesURL, .tags: newFeed.tags.map({$0.id}), .articles: newFeed.articles.map({$0.id})]
				APICall.shared.uploadFromRequest(.createFeed(body)) { (result) in
					switch result {
						case .failure(let err):
							print(err)
						case .success(_):
							picturesURL.forEach { (url) in
								try? FileManager.default.removeItem(at: url)
							}
							self.dismiss(animated: true, completion: nil)
					}
				}
			} else {
				GlobalManager.shared.error(error: errorType.noPicture)
			}
			
		} else {
			
			if let thumbnail = self.newFeed.new_ressources.first { (ressource) -> Bool in
				return ressource.type == .picture
			} {
				let urlThumbnail = documentsFolderURL.appendingPathComponent( String.random() + ".png" )
				
				try? thumbnail.data?.write(to: urlThumbnail)
				
				if let video = self.newFeed.new_ressources.first(where: { (ressource) -> Bool in
					return ressource.type == .video
				}) {
					let urlVideo = documentsFolderURL.appendingPathComponent( String.random() + ".mov")
					
					try? video.data?.write(to: urlVideo)
					
					let body:[APIRequest.APIKeysBody:Any] = [.name : self.newFeed.feed_name, .type: "video", .user: AccountManager.shared.appUser?.id, .video: urlVideo, .picture: urlThumbnail, .tags: newFeed.tags.map({$0.id}), .articles: newFeed.articles.map({$0.id})]
					
					APICall.shared.uploadFromRequest(.createFeed(body)) { (result) in
						switch result {
							case .failure(let err):
								print(err)
							case .success(_):
								try? FileManager.default.removeItem(at: documentsFolderURL)
								self.dismiss(animated: true, completion: nil)
						}
					}
				} else {
					GlobalManager.shared.error(error: errorType.noVideo)
				}
			} else {
				GlobalManager.shared.error(error: errorType.noThumbnail)
			}
		}
	}
	
	func publishTag(didFinish: @escaping () -> Void) {
		if self.newFeed.new_tags.count > 0 {
			self.newFeed.new_tags.forEach { (tag) in
				APICall.shared.uploadFromRequest(.createTag([.name: tag.name])) { (result) in
					switch result {
						case .failure(let err):
							print(err)
						case .success(_):
							let t:[Tag] = APIFetcher.shared.allCached()
							self.newFeed.tags.append(
								contentsOf: t.filter({$0.name == tag.name})
							)
							self.newFeed.new_tags.removeAll { (new_tag) -> Bool in
								return new_tag.name == tag.name
							}
							
							if self.newFeed.new_tags.count == 0 {
								DispatchQueue.main.async {
									didFinish()
								}
							}
					}
				}
			}
		} else {
			DispatchQueue.main.async {
				didFinish()
			}
		}
	}
	
	func publishArticle(didFinish: @escaping () -> Void) {
		
		let documentsFolderURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
		
		if self.newFeed.new_articles.count > 0 {
			self.newFeed.new_articles.forEach { (article) in
				
				var picturesURL = [URL]()
				article.new_ressources.forEach { (ressource) in
					if ressource.type == .picture {
						let path = documentsFolderURL.appendingPathComponent(String.random() + ".png")
						try? ressource.data?.write(to: path)
						picturesURL.append(path)
					}
				}
				
				APICall.shared.uploadFromRequest(.createArticle([.name: article.name, .brand: article.brand, .price: article.price, .picture: picturesURL])) { (result) in
					switch result {
						case .failure(let err):
							print(err)
						case .success(_):
							let t:[Article] = APIFetcher.shared.allCached()
							self.newFeed.articles.append(
								contentsOf: t.filter({$0.name == article.name && $0.brand == article.brand && $0.price == article.price})
							)
							self.newFeed.new_articles.removeAll { (new_article) -> Bool in
								return new_article.name == article.name && new_article.brand == article.brand && new_article.price == article.price
							}
							
							picturesURL.forEach { (url) in
								try? FileManager.default.removeItem(at: url)
							}
							
							if self.newFeed.new_articles.count == 0 {
								DispatchQueue.main.async {
									didFinish()
								}
							}
					}
				}
			}
		} else {
			DispatchQueue.main.async {
				didFinish()
			}
		}
	}
}

class AddInfoNewArticleController:UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, APIFetcherDelegate {
	
	@IBOutlet weak var secondStackViewTags: UIStackView!
	@IBOutlet weak var placeholderTextViewDescription: UILabel!
	@IBOutlet weak var textViewDescription: UITextView!
	@IBOutlet weak var firstStackViewTags: UIStackView!
	@IBOutlet weak var btnPublish: UIButton!
	@IBOutlet weak var searchBarTags: UISearchBar!
	@IBOutlet weak var mainImage: UIImageView!
	@IBOutlet weak var scrollViewTags: UIScrollView!
	@IBOutlet weak var searchBarUsers: UISearchBar!
	@IBOutlet weak var btnBack: UIButton!
	
	var newFeed:NewFeed!
	var tags = [Tag]()
	var users = [User]()
	var articles = [Article]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setup()
		self.request()
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.navigationController?.navigationBar.isHidden = true
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
		self.navigationController?.navigationBar.isHidden = false
	}
	// MARK: Request
	
	func request() {
		self.requestTags()
		self.requestUsers()
	}
	
	func requestTags() {
		self.scrollViewTags.activityIndicator(show: true)
		APIFetcher.shared.request(.tagsMostPopular, delegate: self) { (response) in
			self.tags = APIFetcher.shared.allCached()
			self.tags.sort { (tag_1, tag_2) -> Bool in
				return tag_1.feeds.count > tag_2.feeds.count
			}
			self.scrollViewTags.activityIndicator(show: false)
			self.setupTags()
		}
	}
	
	func requestUsers() {
		AccountManager.shared.appUser?.communities.sorted(by: {
			return Date($0.created_at!)! < Date($1.created_at!)!
		}).forEach({ (community) in
			if (community.type == .follow || community.type == .following) {
				if let user = community.user, !self.users.contains(user) {
					self.users.append(user)
				}
			}
		})
	}
	
	func returnCacheRequest(completion: Error?) {
		print("return cache")
	}
	
	// MARK: Setup
	
	func setup() {
		self.setupContent()
		self.setupGesture()
		self.setupTags()
		self.setupBackgroundColorForSearchBar()
	}
	
	func setupGesture() {
		let tapAddArticle = UITapGestureRecognizer(target: self, action: #selector(self.tapAddArticle(_:)))
		tapAddArticle.numberOfTouchesRequired = 1
		tapAddArticle.numberOfTapsRequired = 1
		
	}
	
	func setupBackgroundColorForSearchBar() {
		let image = UIImage()
		self.getAllSearchBar().forEach { (searchBar) in
			searchBar.backgroundImage = image
			searchBar.backgroundColor = .clear
			searchBar.tintColor = .clear
			searchBar.searchTextField.backgroundColor = .systemBackground
			searchBar.searchTextField.tintColor = .systemBackground
			
			searchBar.searchBarStyle = .prominent
			searchBar.searchBarStyle = .minimal
		}
		
	}
	
	// Tags
	
	func setupTags() {
		
		self.firstStackViewTags.arrangedSubviews.forEach { v in v.removeFromSuperview() }
		self.secondStackViewTags.arrangedSubviews.forEach { v in v.removeFromSuperview() }
		if self.tags.count > 0 {
			for i in 0...self.tags.count - 1 {
				if i%2 == 0 {
					self.firstStackViewTags.addArrangedSubview(createButtonTag(self.tags[i]))
					self.view?.setNeedsLayout()
					self.view?.layoutIfNeeded()
				} else {
					self.secondStackViewTags.addArrangedSubview(createButtonTag(self.tags[i]))
					self.view?.setNeedsLayout()
					self.view?.layoutIfNeeded()
				}
			}
		}
	}
	
	func createButtonTag(_ tag: Tag) -> UIButton {
		
		let b = UIButton(type: .custom)
		b.setTitle(tag.name, for: .normal)
		b.backgroundColor = .systemBackground
		b.setTitleColor(.label, for: .normal)
		b.layer.cornerRadius = 8
		b.titleLabel?.font = UIFont(name: "Poppins-Light", size: 8)
		
		b.titleLabel?.numberOfLines = 1
		b.titleLabel?.adjustsFontSizeToFitWidth = true
		b.titleLabel?.lineBreakMode = .byClipping
		
		b.contentEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
		b.addTarget(self, action: #selector(self.tapTag(_:)), for: .touchUpInside)
		
		return b
	}
	
	// MARK: Setup Images scroll
	
	func setupContent() {
		DispatchQueue.main.async {
			switch self.newFeed?.feed_type {
				case .picture:
					self.mainImage.image = UIImage(data: (self.newFeed.new_ressources.first?.data)!)
				case .video:
					let r = self.newFeed?.new_ressources.first(where: { (ressource) -> Bool in
						return ressource.type == .picture
					})
					self.mainImage.image = UIImage(data: (r?.data)!)
				default: break
			}
		}
	}
	
	// MARK: Delegate UITextField
	
	func textViewDidBeginEditing(_ textView: UITextView) {
		if textView == self.textViewDescription {
			self.placeholderTextViewDescription.isHidden = true
		}
	}
	
	func textViewDidEndEditing(_ textView: UITextView) {
		if textView == self.textViewDescription {
			if self.textViewDescription.text.count == 0 {
				self.placeholderTextViewDescription.isHidden = false
			}
		}
	}
	
	func textViewDidChange(_ textView: UITextView) {
		if textView == self.textViewDescription {
			self.newFeed.feed_name = textView.text
		}
	}
	
	// MARK: Action
	
	@IBAction func backAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@objc func tapTag(_ sender : UIButton) {
		sender.smallAnimateView()
		if sender.backgroundColor == UIColor.systemBackground {
			sender.backgroundColor = .picdiesBlue
			sender.setTitleColor(.white, for: .normal)
			
			if let tag = self.tags.first(where: { (tag) -> Bool in
				return tag.name == sender.titleLabel?.text
			}) {
				self.newFeed.tags.append(tag)
			} else {
				// append at new_tags
			}
			
		} else {
			sender.backgroundColor = .systemBackground
			sender.setTitleColor(.label, for: .normal)
			
			if let tag = self.tags.first(where: { (tag) -> Bool in
				return tag.name == sender.titleLabel?.text
			}) {
				self.newFeed.tags.removeAll { (tag) -> Bool in
					return tag.name == sender.titleLabel?.text
				}
			} else {
				// remove at new_tags
			}
		}
	}
	
	@IBAction func tapPublish(_ sender: Any) {
		self.publishTag {
			self.publishArticle {
				self.publishFeed()
			}
		}
	}
	
	@objc func tapAddArticle(_ sender: UITapGestureRecognizer) {
		sender.view?.smallAnimateView()
	}
	
	
	
}
