//
//  PickerViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 19/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
import AVKit
import Photos


class PickerView:NSObject, YPImagePickerDelegate, APIFetcherDelegate {
	
	
	static let shared = PickerView()
	private override init() {
		super.init()
	}
	
	var selectedItems = [YPMediaItem]()
	var newFeed = NewFeed()
	
	// MARK: - Configuration
	@objc
	func showPicker(with navigation : UINavigationController) {
		var config = YPImagePickerConfiguration()
		/* Uncomment and play around with the configuration 👨‍🔬 🚀 */
		/* Set this to true if you want to force the  library output to be a squared image. Defaults to false */
		//         config.library.onlySquare = true
		/* Set this to true if you want to force the camera output to be a squared image. Defaults to true */
		config.onlySquareImagesFromCamera = false
		/* Ex: cappedTo:1024 will make sure images from the library or the camera will be
		resized to fit in a 1024x1024 box. Defaults to original image size. */
		// config.targetImageSize = .cappedTo(size: 1024)
		/* Choose what media types are available in the library. Defaults to `.photo` */
		config.library.mediaType = .photoAndVideo
		config.library.itemOverlayType = .grid
		config.library.isSquareByDefault = false
		config.library.onlySquare = false
		
		
		/* Enables selecting the front camera by default, useful for avatars. Defaults to false */
		// config.usesFrontCamera = true
		/* Adds a Filter step in the photo taking process. Defaults to true */
		config.showsPhotoFilters = false
		config.showsVideoTrimmer = false
		/* Manage filters by yourself */
		//        config.filters = [YPFilter(name: "Mono", coreImageFilterName: "CIPhotoEffectMono"),
		//                          YPFilter(name: "Normal", coreImageFilterName: "")]
		//        config.filters.remove(at: 1)
		//        config.filters.insert(YPFilter(name: "Blur", coreImageFilterName: "CIBoxBlur"), at: 1)
		/* Enables you to opt out from saving new (or old but filtered) images to the
		user's photo library. Defaults to true. */
		config.shouldSaveNewPicturesToAlbum = true
		
		/* Choose the videoCompression. Defaults to AVAssetExportPresetHighestQuality */
		config.video.compression = AVAssetExportPresetHighestQuality
		config.video.fileType = .mov
		/* Defines the name of the album when saving pictures in the user's photo library.
		In general that would be your App name. Defaults to "DefaultYPImagePickerAlbumName" */
		config.albumName = "picdies"
		/* Defines which screen is shown at launch. Video mode will only work if `showsVideo = true`.
		Default value is `.photo` */
		config.startOnScreen = .video
		
		/* Defines which screens are shown at launch, and their order.
		Default value is `[.library, .photo]` */
		config.screens = [.video, .photo]
		
		/* Can forbid the items with very big height with this property */
		//        config.library.minWidthForItem = UIScreen.main.bounds.width * 0.8
		/* Defines the time limit for recording videos.
		Default is 30 seconds. */
		// config.video.recordingTimeLimit = 5.0
		/* Defines the time limit for videos from the library.
		Defaults to 60 seconds. */
		//		config.video.libraryTimeLimit = 500.0
		
		/* Adds a Crop step in the photo taking process, after filters. Defaults to .none */
		
		/* Defines the overlay view for the camera. Defaults to UIView(). */
		// let overlayView = UIView()
		// overlayView.backgroundColor = .red
		// overlayView.alpha = 0.3
		// config.overlayView = overlayView
		
		/* Defines if the status bar should be hidden when showing the picker. Default is true */
		config.hidesStatusBar = true
		config.preferredStatusBarStyle = .lightContent
		/* Defines if the bottom bar should be hidden when showing the picker. Default is false */
		config.maxCameraZoomFactor = 2.0
		config.library.maxNumberOfItems = 5
		config.gallery.hidesRemoveButton = false
		
		/* Disable scroll to change between mode */
		config.isScrollToChangeModesEnabled = true
		//        config.library.minNumberOfItems = 2
		/* Skip selection gallery after multiple selections */
		// config.library.skipSelectionsGallery = true
		/* Here we use a per picker configuration. Configuration is always shared.
		That means than when you create one picker with configuration, than you can create other picker with just
		let picker = YPImagePicker() and the configuration will be the same as the first picker. */
		
		/* Only show library pictures from the last 3 days */
		//let threDaysTimeInterval: TimeInterval = 3 * 60 * 60 * 24
		//let fromDate = Date().addingTimeInterval(-threDaysTimeInterval)
		//let toDate = Date()
		//let options = PHFetchOptions()
		// options.predicate = NSPredicate(format: "creationDate > %@ && creationDate < %@", fromDate as CVarArg, toDate as CVarArg)
		//
		////Just a way to set order
		//let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: true)
		//options.sortDescriptors = [sortDescriptor]
		//
		//config.library.options = options
		config.library.preselectedItems = selectedItems
		
		// Customise fonts
		config.fonts.pickerTitleFont = UIFont(name: "Roboto-Medium", size: 17)!
		config.fonts.navigationBarTitleFont = UIFont(name: "Roboto-Medium", size: 20)!
		config.fonts.durationFont = UIFont(name: "Poppins-Light", size: 12)!
		config.fonts.albumCellNumberOfItemsFont = UIFont(name: "Poppins-Medium", size: 8)!
		config.fonts.menuItemFont = UIFont(name: "Poppins-Medium", size: 16)!
		config.fonts.rightBarButtonFont = UIFont(name: "Roboto-Regular", size: 16)!
		config.fonts.leftBarButtonFont = UIFont(name: "Roboto-Light", size: 15)!
		
		config.colors.tintColor = .picdiesRed
		config.colors.bottomMenuItemSelectedTextColor = .picdiesRed
		
		// NAVIGATION
		
		let coloredImage = UIImage(ciImage: .clear)
		UINavigationBar.appearance().setBackgroundImage(coloredImage, for: UIBarMetrics.default)
		UINavigationBar.appearance().shadowImage = coloredImage
		
		
		UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.clear ]
		UINavigationBar.appearance().tintColor = .white
		
		// ICONS
		
		if let rotate = UIImage(named: "rotate") {
			config.icons.loopIcon = rotate
		}
		
		if let card = UIImage(named: "cards/cards") {
			config.icons.multipleSelectionOffIcon = card
			config.icons.multipleSelectionOnIcon = card
		}
		
		if let size = UIImage(named: "size/size") {
			config.icons.cropIcon = size
		}
		
		if let flashOff = UIImage(named: "bolt/bolt") {
			config.icons.flashOffIcon = flashOff
		}
		
		if let flashOn = UIImage(named: "bolt/bolt-fill") {
			config.icons.flashOnIcon = flashOn
		}
		
		if let flashAuto = UIImage(named: "bolt/bolt-auto") {
			config.icons.flashAutoIcon = flashAuto
		}
		
		
		if let thumb = UIImage(named: "thumbcamera/thumbcamera") {
			config.icons.capturePhotoImage = thumb
			config.icons.captureVideoImage = thumb
			if let thumbfill = UIImage(named: "thumbcamera/thumbcamera-fill") {
				config.icons.captureVideoOnImage = thumbfill
			}
		}
		
		// LANGUAGE
		
		config.wordings.libraryTitle = "Albums"
		config.wordings.videoTitle = "Vidéo"
		config.wordings.cancel = "Annuler"
		config.wordings.next = "Suivant"
		
		let picker = YPImagePicker(configuration: config)
		picker.imagePickerDelegate = self
		picker.navigationBar.tintColor = .white
		picker.view.window?.backgroundColor = .systemBackground
		picker.didFinishPicking { [unowned picker] items, cancelled in
			
			self.newFeed = NewFeed()
			
			if cancelled {
				print("Picker was canceled")
				picker.dismiss(animated: true, completion: nil)
				return
			}
			
			self.selectedItems = items
			
			items.forEach { (item) in
				switch item {
					case .photo(let photo):
						self.newFeed.feed_type = .picture
						let r = NewRessource(type: .picture, data: photo.image.pngData()!)
						self.newFeed.new_ressources.append(r)
					case .video(let video):
						self.newFeed.feed_type = .video
						let r = NewRessource(type: .video, data: try! Data(contentsOf: video.url))
						self.newFeed.new_ressources.append(r)
						let t = NewRessource(type: .picture, data: video.thumbnail.pngData()!)
						self.newFeed.new_ressources.append(t)
				}
			}
			
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			let vc = storyboard.instantiateViewController(identifier: "AddArticleController") as! AddArticleController
			vc.newFeed = self.newFeed
			picker.pushViewController(vc, animated: true)
		}
		
		/* Single Photo implementation. */
		// picker.didFinishPicking { [unowned picker] items, _ in
		//     self.selectedItems = items
		//     self.selectedImageV.image = items.singlePhoto?.image
		//     picker.dismiss(animated: true, completion: nil)
		// }
		/* Single Video implementation. */
		//picker.didFinishPicking { [unowned picker] items, cancelled in
		//    if cancelled { picker.dismiss(animated: true, completion: nil); return }
		//
		//    self.selectedItems = items
		//    self.selectedImageV.image = items.singleVideo?.thumbnail
		//
		//    let assetURL = items.singleVideo!.url
		//    let playerVC = AVPlayerViewController()
		//    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
		//    playerVC.player = player
		//
		//    picker.dismiss(animated: true, completion: { [weak self] in
		//        self?.present(playerVC, animated: true, completion: nil)
		//        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
		//    })
		//}
		UIApplication.shared.windows.first?.rootViewController?.present(picker, animated: true, completion: nil)
	}
	
	func returnCacheRequest(completion: Error?) {
		print("request")
	}
	
	func noPhotos() {}
	
	func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
		return true
	}
	
	func resolutionForLocalVideo(url: URL) -> CGSize? {
		guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
		let size = track.naturalSize.applying(track.preferredTransform)
		return CGSize(width: abs(size.width), height: abs(size.height))
	}
	
}
