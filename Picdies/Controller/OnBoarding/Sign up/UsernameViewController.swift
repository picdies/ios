//
//  UsernameViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit

class UsernameViewController:UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, APIFetcherDelegate {
	
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var viewArobase: UIView!
	@IBOutlet weak var usernameField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.usernameField.text = UserStorage.newUser[UserStorage.newUserKeys.name.rawValue]
	}
	
	func setupUI() {
		self.viewArobase.border(width: 0.5, color: UIColor.black.withAlphaComponent(0.5), radius: 8)
	}
	
	@IBAction func btnBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@IBAction func goNext(_ sender: Any?) {
		Vibration.selection.vibrate()
		if let username = self.usernameField.text, !username.isEmpty {
			if username.range(of: ".*[^_.A-Za-z0-9].*", options: .regularExpression) == nil {
				self.btnNext?.imageView?.layer.transform = CATransform3DMakeScale(0.0, 0.0, 0.0)
				self.btnNext?.activityIndicator(show: true, style: .medium, color: .white)
				APIFetcher.shared.requestJSON(.checkIfUsernameExist(username), delegate: self) { (result, error) in
					if let err = error { GlobalManager.shared.error(error: errorType.noDetail) }
					else {
						if result?["result"] == false {
							UserStorage.newUser[UserStorage.newUserKeys.name.rawValue] = username
							let registerView = self.storyboard?.instantiateViewController(identifier: "registerViewController") as! RegisterViewController
							self.navigationController?.pushViewController(registerView, animated: true)
						} else { GlobalManager.shared.error(error: errorType.uniqueUsername) }
						self.btnNext?.activityIndicator(show: false)
						self.btnNext?.imageView?.layer.transform = CATransform3DIdentity
					}
				}
			} else { GlobalManager.shared.error(error: errorType.specialCharacters) }
		} else { GlobalManager.shared.error(error: errorType.noUsername) }
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == usernameField { textField.resignFirstResponder() }
		return true
	}
	
	func returnCacheRequest(completion: Error?) { if let error = completion { GlobalManager.shared.error(error: error) } }
	
	
}
