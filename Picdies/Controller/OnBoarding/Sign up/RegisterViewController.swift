//
//  RegisterViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit
import AuthenticationServices

class RegisterViewController:UIViewController, UIGestureRecognizerDelegate, ASAuthorizationControllerPresentationContextProviding {
	
	@IBOutlet weak var viewRegisterGoogle: UIView!
	@IBOutlet weak var viewRegisterFacebook: UIView!
	@IBOutlet weak var viewRegisterApple: UIView!
	@IBOutlet weak var viewRegisterMail: UIView!
	@IBOutlet weak var btnShowPrivacy: UIButton!
	
	private var btnApple:ASAuthorizationAppleIDButton?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}
	
	func setupUI() {
		self.btnShowPrivacy.titleLabel?.numberOfLines = 0
		self.btnShowPrivacy.titleLabel?.lineBreakMode = .byWordWrapping
		
		self.viewRegisterMail.border(width: 0.5, color: .label, radius: 8)
		self.viewRegisterApple.border(width: 0.5, color: .label, radius: 8)
		self.viewRegisterFacebook.border(width: 0.5, color: .label, radius: 8)
		self.viewRegisterGoogle.border(width: 0.5, color: .label, radius: 8)
		
		if #available(iOS 13.2, *) {
			if self.btnApple == nil {
				let b = ASAuthorizationAppleIDButton(type: .signIn, style: traitCollection.userInterfaceStyle == .light ? .white : .black)
				b.cornerRadius = 8.0
				b.addTarget(self, action: #selector(self.openAppleRegister(_:)), for: .touchUpInside)
				self.viewRegisterApple.addSubview(b)
				self.btnApple = b
			}
			self.btnApple?.frame = self.viewRegisterApple.bounds
		} else {
			self.viewRegisterApple.removeFromSuperview()
		}
		
		self.addTapGesture()
	}
	
	func addTapGesture() {
		let gestureMail = UITapGestureRecognizer(target: self, action: #selector(self.openMailRegister(_:)))
		let gestureGoogle = UITapGestureRecognizer(target: self, action: #selector(self.openGoogleRegister(_:)))
		let gestureFacebook = UITapGestureRecognizer(target: self, action: #selector(self.openFacebookRegister(_:)))
		
		self.viewRegisterMail.addGestureRecognizer(gestureMail)
		self.viewRegisterGoogle.addGestureRecognizer(gestureGoogle)
		self.viewRegisterFacebook.addGestureRecognizer(gestureFacebook)
	}
	
	@IBAction func btnBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	// MARK: Mail
	
	@objc func openMailRegister(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
		let registerMailView = self.storyboard?.instantiateViewController(identifier: "emailRegisterViewController") as! EmailRegisterViewController
		self.navigationController?.pushViewController(registerMailView, animated: true)
	}
	
	
	// MARK: Google
	
	@objc func openGoogleRegister(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
		AccountManager.shared.register(with: .google, and: [:]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				let successView = self.storyboard?.instantiateViewController(identifier: "successRegisterViewController") as! SuccessRegisterViewController
				self.navigationController?.pushViewController(successView, animated: true)
			}
		}
	}
	
	
	// MARK: Facebook
	
	@objc func openFacebookRegister(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
		AccountManager.shared.register(with: .facebook, and: [:]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				let successView = self.storyboard?.instantiateViewController(identifier: "successRegisterViewController") as! SuccessRegisterViewController
				self.navigationController?.pushViewController(successView, animated: true)
			}
		}
	}
	
	// MARK: Apple
	
	@objc func openAppleRegister(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIButton { b.animateView(withCompletion: {})}
		AccountManager.shared.register(with: .apple, and: [.delegate:self]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				let successView = self.storyboard?.instantiateViewController(identifier: "successRegisterViewController") as! SuccessRegisterViewController
				self.navigationController?.pushViewController(successView, animated: true)
			}
		}
	}
	
	func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
		self.view.window!
	}
	
}
