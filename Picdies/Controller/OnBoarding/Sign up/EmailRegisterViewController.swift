//
//  EmailRegisterViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit

class EmailRegisterViewController:UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, APIFetcherDelegate {
	
	@IBOutlet weak var mailField: UITextField!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var passwordField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.mailField.text = UserStorage.newUser[UserStorage.newUserKeys.email.rawValue]
		self.passwordField.text = UserStorage.newUser[UserStorage.newUserKeys.password.rawValue]
	}
	
	func setupUI() { }
	
	@IBAction func btnBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@IBAction func goNext(_ sender: Any?) {
		Vibration.selection.vibrate()
		if let email = self.mailField.text, !email.isEmpty {
			if let password = self.passwordField.text, !password.isEmpty {
				self.btnNext?.imageView?.layer.transform = CATransform3DMakeScale(0.0, 0.0, 0.0)
				self.btnNext?.activityIndicator(show: true, style: .medium, color: .white)
				AccountManager.shared.register(with: .email, and: [.email: self.mailField.text!, .password: self.passwordField.text!]) { (result) in
					self.responseCreateUser(response: result)
				}
			} else { GlobalManager.shared.error(error: errorType.noPassword) }
		} else { GlobalManager.shared.error(error: errorType.noEmail) }
	}
	
	func responseCreateUser(response : Error?) {
		self.btnNext?.activityIndicator(show: false)
		self.btnNext?.imageView?.layer.transform = CATransform3DIdentity
		if let error = response {
			GlobalManager.shared.error(error: error)
			return
		} else {
			let successView = self.storyboard?.instantiateViewController(identifier: "successRegisterViewController") as! SuccessRegisterViewController
			self.navigationController?.pushViewController(successView, animated: true)
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseCreateUser(response: completion)
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == mailField {
			textField.resignFirstResponder()
			passwordField.becomeFirstResponder()
		} else if textField == passwordField {
			passwordField.resignFirstResponder()
		}
		return true
	}
}
