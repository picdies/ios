//
//  SuccessRegisterViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit

class SuccessRegisterViewController:UIViewController, UIGestureRecognizerDelegate {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}
	
	func setupUI() { }
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@IBAction func goNext(_ sender: Any?) {
		Vibration.selection.vibrate()
	}
	
	@IBAction func goSkip(_ sender: Any?) {
		Vibration.selection.vibrate()
		UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
	}
	
}
