//
//  PresentationSliderViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit

class PresentationSliderViewController:UIViewController, UIScrollViewDelegate {
	
	@IBOutlet weak var stackViewContent: UIStackView!
	@IBOutlet weak var stackViewPagination: UIStackView!
	@IBOutlet weak var btnConnect: UIButton!
	@IBOutlet weak var btnStart: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupStackViewContent()
		self.updatePagination(actualPage: 0)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}

	func setupStackViewContent() {
		self.stackViewContent.arrangedSubviews.forEach { (view) in
			view.translatesAutoresizingMaskIntoConstraints = false
			let widthConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width)
			view.addConstraints([widthConstraint])
		}
	}
	
	@IBAction func clicStart(_ sender: Any) {
		if let b = sender as? UIButton { b.animateButton(color: nil, image: nil, withCompletion: {})}
		Vibration.selection.vibrate()
		let whoAreYouView = self.storyboard?.instantiateViewController(identifier: "whoAreYouViewController") as! WhoAreYouViewController
		self.navigationController?.pushViewController(whoAreYouView, animated: true)
	}

	@IBAction func clicConnect(_ sender: Any) {
		if let b = sender as? UIButton { b.animateButton(color: nil, image: nil, withCompletion: {})}
		let connectView = self.storyboard?.instantiateViewController(identifier: "connectViewController") as! ConnectViewController
		self.navigationController?.pushViewController(connectView, animated: true)
	}

	// MARK: Pagination

	func updatePagination(actualPage: Int) {
		UIView.animate(withDuration: 0.4, animations: {
			self.stackViewPagination.arrangedSubviews[0].alpha = actualPage == 0 ? 1 : 0.35
			self.stackViewPagination.arrangedSubviews[1].alpha = actualPage == 1 ? 1 : 0.35
			self.stackViewPagination.arrangedSubviews[2].alpha = actualPage == 2 ? 1 : 0.35
		})
	}

	// MARK: Scroll

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let currentPage = scrollView.currentPage
		self.updatePagination(actualPage: currentPage)
	}

}

