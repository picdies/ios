//
//  ConnectViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit
import AuthenticationServices

class ConnectViewController:UIViewController, UIGestureRecognizerDelegate, ASAuthorizationControllerPresentationContextProviding{

	@IBOutlet weak var viewConnectGoogle: UIView!
	@IBOutlet weak var viewConnectFacebook: UIView!
	@IBOutlet weak var viewConnectApple: UIView!
	@IBOutlet weak var viewConnectMail: UIView!
	@IBOutlet weak var btnShowPrivacy: UIButton!

	private var btnApple: ASAuthorizationAppleIDButton?

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}

	func setupUI() {
		self.btnShowPrivacy.titleLabel?.numberOfLines = 0
		self.btnShowPrivacy.titleLabel?.lineBreakMode = .byWordWrapping

		self.viewConnectMail.border(width: 0.5, color: .label, radius: 8)
		self.viewConnectApple.border(width: 0.5, color: .label, radius: 8)
		self.viewConnectFacebook.border(width: 0.5, color: .label, radius: 8)
		self.viewConnectGoogle.border(width: 0.5, color: .label, radius: 8)

		if #available(iOS 13.2, *) {
			if self.btnApple == nil {
				let b = ASAuthorizationAppleIDButton(type: .signUp, style: traitCollection.userInterfaceStyle == .light ? .white : .black)
				b.cornerRadius = 8.0
				b.addTarget(self, action: #selector(self.openAppleConnect(_:)), for: .touchUpInside)
				self.viewConnectApple.addSubview(b)
				self.btnApple = b
			}
			self.btnApple?.frame = self.viewConnectApple.bounds
		} else {
			self.viewConnectApple.removeFromSuperview()
		}

		self.addTapGesture()

	}

	@IBAction func btnBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}

	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
			return true
		}

	// MARK: Gesture

	func addTapGesture() {
		let gestureMail = UITapGestureRecognizer(target: self, action: #selector(self.openMailConnect(_:)))
		let gestureGoogle = UITapGestureRecognizer(target: self, action: #selector(self.openGoogleConnect(_:)))
		let gestureFacebook = UITapGestureRecognizer(target: self, action: #selector(self.openFacebookConnect(_:)))

		self.viewConnectMail.addGestureRecognizer(gestureMail)
		self.viewConnectGoogle.addGestureRecognizer(gestureGoogle)
		self.viewConnectFacebook.addGestureRecognizer(gestureFacebook)
	}

	// MARK: Mail

	@objc func openMailConnect(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
	}

	// MARK: Google

	@objc func openGoogleConnect(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
		AccountManager.shared.login(with: .google, and: [:]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
			}
		}
	}

	// MARK: Facebook

	@objc func openFacebookConnect(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIView { b.animateView(withCompletion: {})}
		AccountManager.shared.login(with: .facebook, and: [:]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
			}
		}
	}

	// MARK: Apple

	@objc func openAppleConnect(_ sender:Any?) {
		Vibration.selection.vibrate()
		if let b = sender as? UIButton { b.animateView(withCompletion: {})}
		AccountManager.shared.login(with: .apple, and: [.delegate:self]) { (response) in
			if let error = response {
				GlobalManager.shared.error(error: error)
				return
			} else {
				UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
			}
		}
	}

	func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
		self.view.window!
	}
}
