//
//  WhoAreYouViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit
import DatePickerDialog

class WhoAreYouViewController:UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate {
	
	@IBOutlet weak var birthdayField: UITextField!
	@IBOutlet weak var lastNameField: UITextField!
	@IBOutlet weak var firstNameField: UITextField!
	let datePicker = DatePickerDialog(locale: Locale.preferredLocale())
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
		self.setupUI()
		self.navigationController?.interactivePopGestureRecognizer?.delegate = self
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.firstNameField.text = UserStorage.newUser[UserStorage.newUserKeys.first_name.rawValue]
		self.lastNameField.text = UserStorage.newUser[UserStorage.newUserKeys.last_name.rawValue]
		self.birthdayField.text = UserStorage.newUser[UserStorage.newUserKeys.birthday.rawValue]
	}
	
	func setupUI() { }
	
	@IBAction func btnBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
	
	@IBAction func goNext(_ sender: Any?) {
		Vibration.selection.vibrate()
		if let firstname = self.firstNameField.text, !firstname.isEmpty {
			if let lastname = self.lastNameField.text, !lastname.isEmpty {
				if let birthday = self.birthdayField.text, !birthday.isEmpty {
					UserStorage.newUser[UserStorage.newUserKeys.first_name.rawValue] = firstname
					UserStorage.newUser[UserStorage.newUserKeys.last_name.rawValue] = lastname
					UserStorage.newUser[UserStorage.newUserKeys.birthday.rawValue] = birthday
					let usernameView = self.storyboard?.instantiateViewController(identifier: "usernameViewController") as! UsernameViewController
					self.navigationController?.pushViewController(usernameView, animated: true)
				} else { GlobalManager.shared.error(error: errorType.noBirthday) }
			} else { GlobalManager.shared.error(error: errorType.noLastName) }
		} else { GlobalManager.shared.error(error: errorType.noFirstName) }
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == firstNameField {
			textField.resignFirstResponder()
			lastNameField.becomeFirstResponder()
		} else if textField == lastNameField {
			textField.resignFirstResponder()
			birthdayField.becomeFirstResponder()
		} else if textField == birthdayField {
			textField.resignFirstResponder()
		}
		return true
	}
	
	func datePickerTapped() {
		datePicker.show("Date de naissance",
						doneButtonTitle: "Valider",
						cancelButtonTitle: "Annuler",
						maximumDate: Date(),
						datePickerMode: .date) { (date) in
			if let dt = date {
				let formatter = DateFormatter()
				formatter.dateFormat = "dd/MM/yyyy"
				self.birthdayField.text = formatter.string(from: dt)
			}
		}
	}
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == self.birthdayField {
			datePickerTapped()
			return false
		}
		return true
	}
	
}
