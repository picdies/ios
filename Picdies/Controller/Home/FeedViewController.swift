//
//  FeedViewController.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class FeedViewController: AVPlayerViewController, StoryboardScene, OverlayPlayerDelegate, ProgressIndicatorHUDPresenter, APIFetcherDelegate {

    static var sceneStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var index: Int!
    var feed: Feed!
    var isPlaying: Bool!
	var	overlay: OverlayPlayer!
	var timeObserverToken:Any!
	var articleIsOpen:Bool = false
	var isPaging:Bool = false

	override var preferredStatusBarStyle : UIStatusBarStyle {
		return .lightContent
	}

	static func instantiate(feed: Feed, andIndex index: Int, isPlaying: Bool = false, isPaging:Bool = false) -> UIViewController {
        let viewController = FeedViewController.instantiate()
        viewController.feed = feed
        viewController.index = index
		viewController.isPaging = isPaging
        viewController.isPlaying = isPlaying
		viewController.player = AVPlayer()
		viewController.showsPlaybackControls = false
		viewController.videoGravity = .resizeAspectFill
		viewController.player?.automaticallyWaitsToMinimizeStalling = false
		DispatchQueue.main.async {
			let o = OverlayPlayer.makeFromNib()
			o.setupUI(feed: feed, view: viewController.contentOverlayView!, delegate: viewController, isPaging: isPaging)
			viewController.contentOverlayView?.addSubview(o)
			viewController.overlay = o

			let tap = UITapGestureRecognizer(target: viewController, action: #selector(self.showArticle))
			tap.numberOfTapsRequired = 1

			let doubleTap = UITapGestureRecognizer(target: viewController, action: #selector(self.tapLike))
			doubleTap.numberOfTapsRequired = 2

			let pressTap = UILongPressGestureRecognizer(target: viewController, action: #selector(self.controlAudioPlayer))
			pressTap.minimumPressDuration = 0.3

			tap.require(toFail: doubleTap)

			viewController.overlay?.scrollViewContent.addGestureRecognizer(pressTap)
			viewController.overlay?.scrollViewContent.addGestureRecognizer(tap)
			viewController.overlay?.scrollViewContent.addGestureRecognizer(doubleTap)
		}
        return viewController
    }

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.overlay?.layoutIfNeeded()
		UIView.animate(withDuration: 0.2) {
			self.setNeedsStatusBarAppearanceUpdate()
		}
	}

	func returnCacheRequest(completion: Error?) {
		self.overlay.setupData()
	}
	
	@objc func showArticle() {
		if self.articleIsOpen {
			overlay?.heightScrollView.constant = 0
			self.articleIsOpen = false
			UIView.animate(withDuration: 0.2) {
				if self.player?.isPlaying ?? false || self.player == nil {
					self.overlay?.opacifier.alpha = 0
				}
				self.view.layoutIfNeeded()
			}
		} else {
			UIView.animate(withDuration: 0) {
				self.overlay?.setupStackViewArticle()
			} completion: { (finish) in
				UIView.animate(withDuration: 0.2) {
					self.overlay?.heightScrollView.constant = CGFloat(self.overlay.heightOfBottomView)
					if self.player?.isPlaying ?? false || self.player == nil {
						self.overlay?.opacifier.alpha = 0.5
					}
					self.view.layoutIfNeeded()
				}
				self.articleIsOpen = true
			}
		}

	}

	@objc func playerDidFinishPlaying() {
		self.player?.seek(to: .zero)
		self.player?.play()
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		self.view.backgroundColor = .systemGray5
		initializeFeed()
    }

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(true)
		player?.seek(to: .zero)
		player?.pause()
		player?.isMuted = true
		player?.currentItem?.preferredPeakBitRate = 2000
		player?.currentItem?.preferredForwardBufferDuration = 3
		player?.currentItem?.preferredMaximumResolution = CGSize(width: 480, height: 640)
		NotificationCenter.default.removeObserver(self)
	}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
		self.player?.pause()
		self.player?.isMuted = false
		addPeriodicTimeObserver()
		observePlayer()
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(true)
		self.player?.playImmediately(atRate: 1.0)
		self.player?.currentItem?.preferredPeakBitRate = .zero
		self.player?.currentItem?.preferredForwardBufferDuration = .zero
		self.player?.currentItem?.preferredMaximumResolution = .zero
	}
   
    func initializeFeed() {
		switch feed.type {
			case .picture:
				player = nil
			case .video:
				let playerItem = AVPlayerItem(asset: feed.asset ?? AVURLAsset(url: (feed.ressources.first?.url)!))
				playerItem.preferredPeakBitRate = 2000
				playerItem.preferredForwardBufferDuration = 3
				playerItem.preferredMaximumResolution = CGSize(width: 480, height: 640)
				player?.replaceCurrentItem(with: playerItem)
				player?.isMuted = true
				player?.play()
			default:
				break
		}
    }

	@objc func controlAudioPlayer(sender : UILongPressGestureRecognizer) {
		if sender.state == .began {
			self.player?.pause()
			self.hideAllView(hidden: true)
		}
		if sender.state == .ended {
			self.player?.play()
			self.hideAllView(hidden: false)
		}
	}

	func hideAllView(hidden : Bool) {
		for view in self.overlay.subviews.filter({ (view) -> Bool in
			return view.tag != 999 || (view.tag != 998 && self.feed.type == .video)
		}) {
			view.isHidden = hidden
		}
		if self.isPaging { self.overlay.backBtn.isHidden = true }
		UIApplication.shared.windows.first?.rootViewController?.view.viewWithTag(12345)?.isHidden = hidden
		UIApplication.shared.windows.first?.rootViewController?.view.viewWithTag(123456)?.isHidden = hidden
	}


	// MARK: Delegate overlay

	@objc func tapLike() {
		var image = UIImage(named: "like/like-fill")
		var tint = UIColor.picdiesRed
		if UserStorage.likedFeed.contains(self.feed.id) {
			image = UIImage(named: "like/like")
			tint = .white
			UserStorage.likedFeed.removeAll(where: {$0 == self.feed.id})
			feed.interaction?.like! -= 1
			if let id = self.feed?.interaction?.id {
				APIFetcher.shared.request(.updateFeedLike(id, [.like:-1]), delegate: self) { (response) in }
			}
		} else {
			Vibration.soft.vibrate()
			UserStorage.likedFeed.append(self.feed.id)
			feed.interaction?.like! += 1
			if let id = self.feed?.interaction?.id {
				APIFetcher.shared.request(.updateFeedLike(id, [.like:1]), delegate: self) { (response) in }
			}
		}
		if let button = overlay.likeBtn {
			button.animateButton(color : tint, image : image!) {
				self.overlay.setupData()
			}
		}


	}

	func tapComment() {
		print("tap comment")
	}

	func tapDismiss() {
		self.navigationController?.popViewController(animated: true)
	}

	func tapShare() {
		let activity = Sharing.shared.openSharing(title: self.feed.name ?? "", description: "@" + (self.feed.user?.name ?? "") + " sur Picdies", url: (self.feed.ressources.first?.url)!)
		activity.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) -> Void in
			if completed == true {
				self.feed.interaction?.share! += 1
				if let id = self.feed?.interaction?.id {
					APIFetcher.shared.request(.updateFeedShare(id, [.share:1]), delegate: self) { (response) in }
				}
				self.overlay.setupData()
			}
		}
		self.present(activity, animated: true, completion: {})
	}

	func tapShowArticle() {
		self.showArticle()
	}

	func tapArticle(article: Article) {
		let articleVC = self.storyboard?.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
		articleVC.article = article
		articleVC.modalPresentationStyle = .fullScreen
		self.navigationController?.present(articleVC, animated: true)
	}

	// MARK: Update Time

	func addPeriodicTimeObserver() {
		let timeScale = CMTimeScale(NSEC_PER_SEC)
		let time = CMTime(seconds: 0.5, preferredTimescale: timeScale)
		timeObserverToken = player?.addPeriodicTimeObserver(forInterval: time, queue: .main) {
			[weak self] time in
			if !time.isIndefinite {
				if let t:CMTime = ((self?.player?.currentItem?.duration ?? CMTime()) - time) as? CMTime {
					if !(t.isIndefinite ) {
						if self?.isReadyForDisplay ?? false {
							self?.overlay?.thumbnailImage?.removeFromSuperview()
						}
					}
				}
			}
		}
	}

	var playbackLikelyToKeepUpKeyPathObserver: NSKeyValueObservation?
	var playbackBufferEmptyObserver: NSKeyValueObservation?
	var playbackBufferFullObserver: NSKeyValueObservation?

	private func observePlayer() {
		NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
	}


	func removePeriodicTimeObserver() {
		if let timeObserverToken = timeObserverToken {
			player?.removeTimeObserver(timeObserverToken)
			self.timeObserverToken = nil
		}
	}


	func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}

	func tapUser() {
		let vc = self.storyboard?.instantiateViewController(identifier: "AccountViewController") as! AccountViewController
		vc.user = self.feed.user
		self.navigationController?.pushViewController(vc, animated: true)

	}
}


