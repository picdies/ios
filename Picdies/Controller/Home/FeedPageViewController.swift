//
//  FeedPageViewController.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import UIKit

typealias IndexedFeed = (feed: Feed, index: Int)

protocol FeedPageView: class, ProgressIndicatorHUDPresenter {
    func presentInitialFeed(_ feed: Feed)
}

class FeedPageViewController: UIPageViewController, FeedPageView {

    var presenter: FeedPagePresenterProtocol!
	var	feedView = [FeedViewController]()

    func presentInitialFeed(_ feed: Feed) {
		let viewController = FeedViewController.instantiate(feed: feed, andIndex: 0, isPlaying: true, isPaging: true) as! FeedViewController
		self.feedView.append(viewController)
        setViewControllers([viewController], direction: .forward, animated: false, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
		view.backgroundColor = .systemBackground
		self.createTopTitle()
		self.createNewButton()
        presenter = FeedPagePresenter(view: self)
        presenter.viewDidLoad()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(true)
	}

	override func viewDidLayoutSubviews() {
		UIView.animate(withDuration: 0.2) {
			self.setNeedsStatusBarAppearanceUpdate()
		}
	}

	override var preferredStatusBarStyle : UIStatusBarStyle {
		return .lightContent
	}
	
	func createTopTitle() {
		let title = UILabel(frame: CGRect(x: 16, y: 56, width: 200, height: 40))
		title.text = "picdies"
		title.tag = 12345
		title.font = UIFont(name: "Poppins-Medium", size: 30)
		title.textColor = .white
		self.view.addSubview(title)
	}

	@objc func openPicker(_ sender:Any) {
		if let b = sender as? UIButton { b.animateButton(color: nil, image: nil, withCompletion: {})}
		PickerView.shared.showPicker(with: self.navigationController!)
	}

	func createNewButton() {
		let button = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 48, y: 61, width: 32, height: 32))
		button.tag = 123456
		button.addTarget(self, action: #selector(self.openPicker(_:)), for: .touchUpInside)
		button.setImage(UIImage(systemName: "plus"), for: .normal)
		button.setPreferredSymbolConfiguration(.init(pointSize: 20, weight: .regular, scale: UIImage.SymbolScale.large), forImageIn: .normal)
		button.tintColor = .white
		self.view.addSubview(button)
	}
	
}


extension FeedPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let indexedFeed = presenter.fetchPreviousFeed() else {
            return nil
        }
		if let v = self.feedView.first(where: { (feedView) -> Bool in
			return feedView.feed.id == indexedFeed.feed.id
		}) {
			return v
		}
		let v = FeedViewController.instantiate(feed: indexedFeed.feed, andIndex: indexedFeed.index, isPaging: true)
		feedView.append(v as! FeedViewController)
		return v
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let indexedFeed = presenter.fetchNextFeed() else {
            return nil
        }
		if let v = self.feedView.first(where: { (feedView) -> Bool in
			return feedView.feed.id == indexedFeed.feed.id
		}) {
			return v
		}
		let v = FeedViewController.instantiate(feed: indexedFeed.feed, andIndex: indexedFeed.index, isPaging: true)
		feedView.append(v as! FeedViewController)
		return v
	}
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        if let viewController = pageViewController.viewControllers?.first as? FeedViewController
		{
			presenter.updateFeedIndex(fromIndex: viewController.index)
		}
    }

}


