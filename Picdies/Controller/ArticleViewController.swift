//
//  ArticleViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 07/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import MXParallaxHeader

class ArticleViewController:UIViewController, UIScrollViewDelegate{
	
	@IBOutlet weak var mainScrollView: UIScrollView!
	@IBOutlet weak var mainView: UIView!
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var brandLabel: UILabel!
	@IBOutlet weak var settingsBtn: UIButton!
	@IBOutlet weak var backBtn: UIButton!
	@IBOutlet weak var titleArticleHeader: UILabel!
	@IBOutlet weak var imageArticle: UIImageView!
	@IBOutlet weak var viewHeader: UIView!
	
	var article:Article!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.setup()
	}
	
	func setupImageScrollZoom() {
		self.mainScrollView.parallaxHeader.view = self.viewHeader
		self.mainScrollView.parallaxHeader.height = self.viewHeader.bounds.height
		self.mainScrollView.parallaxHeader.mode = .fill
		self.mainScrollView.parallaxHeader.minimumHeight = 56 + (UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0)
	}
	
	func setupUI() {
		self.viewHeader.alpha = 1
		self.imageArticle.blurView.setup(style: .dark, alpha: 0).enable()
		self.backBtn.layer.cornerRadius = self.backBtn.layer.frame.height / 2
		self.settingsBtn.layer.cornerRadius = self.settingsBtn.layer.frame.height / 2
	}
	
	
	func setup() {
		self.setupUI()
		self.setupImageScrollZoom()
		self.imageArticle?.sd_setImage(with: self.article.ressources.first?.url, completed: {_,_,_,_ in })
		self.nameLabel?.text = self.article.name
		self.titleArticleHeader?.text = self.article.name
		self.brandLabel?.text = self.article.brand
		self.priceLabel?.text = (self.article.price ?? "") + " €"
	}
	
	@IBAction func backAction(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView == self.mainScrollView {
			if scrollView.contentOffset.y < 0 {
				let alpha = (((UIScreen.main.bounds.width) - abs(scrollView.contentOffset.y)) / (UIScreen.main.bounds.width))
				self.imageArticle.blurView.alpha = alpha * 0.75
				self.titleArticleHeader.alpha = alpha
			}
		}
	}
	
	
}
