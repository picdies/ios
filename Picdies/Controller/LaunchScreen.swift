//
//  LaunchScreen.swift
//  Picdies
//
//  Created by Thomas Gallier on 20/01/2021.
//

import Foundation
import UIKit
import Firebase

class LaunchScreen:UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		DispatchQueue.main.async {
			self.instantiateMainStoryboard()
		}
	}
	
	func instantiateMainStoryboard() {
		if AccountManager.shared.appUser == nil {
			if let u = Auth.auth().currentUser {
				AccountManager.shared.connectUser(u) { (error) in
					if error == nil { self.goMain() }
					else { self.goOnboarding(waiting: false) }
				}
			} else { self.goOnboarding(waiting: true) }
		} else { self.goOnboarding(waiting: true) }
		Ads.shared.requestAds(delegate: self)
	}
	
	func goOnboarding(waiting : Bool) {
		DispatchQueue.main.asyncAfter(deadline: .now() + (waiting ? 1.5 : 0)) {
			UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "OnBoarding", bundle: Bundle.main).instantiateInitialViewController()
		}
	}
	
	func goMain() {
		UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
	}
}
