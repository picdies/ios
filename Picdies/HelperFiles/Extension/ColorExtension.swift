//
//  ColorExtension.swift
//  Picdies
//
//  Created by Thomas Gallier on 24/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

	static var random: UIColor {
		return UIColor(red: .random(in: 0...1),
					   green: .random(in: 0...1),
					   blue: .random(in: 0...1),
					   alpha: 1.0)
	}

	open class var picdiesRed: UIColor { return UIColor(red: 255 / 255.0, green: 109 / 255.0, blue: 109 / 255.0, alpha: 1) }
	open class var picdiesBlue: UIColor { return UIColor(red: 138 / 255.0, green: 201 / 255.0, blue: 241 / 255.0, alpha: 1) }

}

extension UISearchBar {
  func changeSearchBarColor(color: UIColor) {
		UIGraphicsBeginImageContext(self.frame.size)
		color.setFill()
		UIBezierPath(roundedRect: self.frame, cornerRadius: 8).fill()
		let bgImage = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		self.setSearchFieldBackgroundImage(bgImage, for: .normal)
	}
}


