//
//  NavigationController.swift
//  Picdies
//
//  Created by Thomas Gallier on 04/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation
import UIKit


class NavigationController : UINavigationController {
	override var preferredStatusBarStyle : UIStatusBarStyle {
		return topViewController?.preferredStatusBarStyle ?? .default
	}
	override open var childForStatusBarStyle: UIViewController? {
		return topViewController
	}
}

class TabBarController : UITabBarController {
	override var preferredStatusBarStyle : UIStatusBarStyle {
		return viewControllers?.last?.preferredStatusBarStyle ?? .default
	}
	override open var childForStatusBarStyle: UIViewController? {
		return viewControllers?.last
	}
}

class TabBar:UITabBar {
//	override open func sizeThatFits(_ size: CGSize) -> CGSize {
//		super.sizeThatFits(size)
//		var sizeThatFits = super.sizeThatFits(size)
//		sizeThatFits.height = 70
//		return sizeThatFits
//	}
}


