//
//  ElementExtension.swift
//  Picdies
//
//  Created by Thomas Gallier on 22/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import CoreGraphics
import CryptoKit

extension String {

	static func random(length: Int = 20) -> String {
		let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		var randomString: String = ""

		for _ in 0..<length {
			let randomValue = arc4random_uniform(UInt32(base.count))
			randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
		}
		return randomString
	}

	internal func sha256() -> String { return SHA256.hash(data: Data(self.utf8)).compactMap { return String(format: "%02x", $0) }.joined() }


	static func randomNonce(length: Int = 32) -> String {
		precondition(length > 0)
		let charset: [Character] = Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
		var result = ""
		var remainingLength = length

		while remainingLength > 0 {
			let randoms: [UInt8] = (0 ..< 16).map { _ in
				var random: UInt8 = 0
				let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
				if errorCode != errSecSuccess { fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)") }
				return random
			}

			randoms.forEach { random in
				if remainingLength == 0 { return }

				if random < charset.count {
					result.append(charset[Int(random)])
					remainingLength -= 1
				}
			}
		}
		return result
	}
}

extension UIView {
	func border(width : Double?, color: UIColor?, radius: Double?) {
		if width != nil { self.layer.borderWidth = CGFloat(width!) }
		if color != nil { self.layer.borderColor = color?.cgColor }
		if radius != nil { self.layer.cornerRadius = CGFloat(radius!) }
	}

}
enum Vibration {
   case error
   case success
   case warning
   case light
   case medium
   case heavy
   @available(iOS 13.0, *)
   case soft
   @available(iOS 13.0, *)
   case rigid
   case selection
   case oldSchool

   public func vibrate() {
	   switch self {
	   case .error:
		   UINotificationFeedbackGenerator().notificationOccurred(.error)
	   case .success:
		   UINotificationFeedbackGenerator().notificationOccurred(.success)
	   case .warning:
		   UINotificationFeedbackGenerator().notificationOccurred(.warning)
	   case .light:
		   UIImpactFeedbackGenerator(style: .light).impactOccurred()
	   case .medium:
		   UIImpactFeedbackGenerator(style: .medium).impactOccurred()
	   case .heavy:
		   UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
	   case .soft:
		   if #available(iOS 13.0, *) {
			   UIImpactFeedbackGenerator(style: .soft).impactOccurred()
		   }
	   case .rigid:
		   if #available(iOS 13.0, *) {
			   UIImpactFeedbackGenerator(style: .rigid).impactOccurred()
		   }
	   case .selection:
		   UISelectionFeedbackGenerator().selectionChanged()
	   case .oldSchool:
		   AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
	   }
   }
}

extension UIButton {
	@objc func animateButton(color:UIColor?, image: UIImage?, withCompletion: @escaping () -> Void) {
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			self.alpha = 0.8
			self.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
			if color != nil { self.tintColor = color }
		}) { finished in
			UIView.animate(withDuration: 0.2, delay: 0, options: .allowUserInteraction, animations: {
				self.alpha = 1
				self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
				if image != nil { self.setImage(image, for: .normal) }
			}) { finished in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				 self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
				}) { _ in
					DispatchQueue.main.async {
						withCompletion()
					}
				}
			}
		}
	}
}

extension UIView {
	@objc func animateView(withCompletion: @escaping () -> Void) {
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			self.alpha = 0.8
			self.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
		}) { finished in
			UIView.animate(withDuration: 0.2, delay: 0, options: .allowUserInteraction, animations: {
				self.alpha = 1
				self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
			}) { finished in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				 self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
				}) { _ in
					DispatchQueue.main.async {
						withCompletion()
					}
				}
			}
		}
	}

	@objc func smallAnimateView() {
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			self.alpha = 0.8
			self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
		}) { finished in
			UIView.animate(withDuration: 0.2, delay: 0, options: .allowUserInteraction, animations: {
				self.alpha = 1
				self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
			}) { finished in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				 self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
				})
			}
		}
	}

}

extension Locale {
	static func preferredLocale() -> Locale {
		guard let preferredIdentifier = Locale.preferredLanguages.first else {
			return Locale.current
		}
		return Locale(identifier: preferredIdentifier)
	}
}

extension AVPlayer {
	var isPlaying:Bool {
		return (self.rate != 0 && self.error == nil)
	}
}

extension Int {

	func secondsToTime() -> String {

		let (m,s) = ((self % 3600) / 60, (self % 3600) % 60)

		let m_string =  m < 10 ? "\(m)" : "\(m)"
		let s_string =  s < 10 ? "0\(s)" : "\(s)"

		return "\(m_string):\(s_string)"
	}
}

extension CMTime {
	var roundedSeconds: TimeInterval {
		return seconds.rounded()
	}
	var hours:  Int { return Int(roundedSeconds / 3600) }
	var minute: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 3600) / 60) }
	var second: Int { return Int(roundedSeconds.truncatingRemainder(dividingBy: 60)) }
	var positionalTime: String {
		return hours > 0 ?
			String(format: "%d:%02d:%02d",
				   hours, minute, second) :
			String(format: "%02d:%02d",
				   minute, second)
	}
}

extension URL {
	func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
			DispatchQueue.global().async {
				let asset = AVAsset(url: self)
				let imageGenerator = AVAssetImageGenerator(asset: asset)
				let time = CMTime(seconds: 0.0, preferredTimescale: 600)
				let times = [NSValue(time: time)]
				imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
					if let image = image {
						completion(UIImage(cgImage: image))
					} else {
						completion(nil)
					}
				})
			}
		}
}

