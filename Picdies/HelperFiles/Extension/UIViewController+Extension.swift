//
//  UIViewController+Extension.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {

	class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

		if let nav = base as? UINavigationController {
			return getTopViewController(base: nav.visibleViewController)

		} else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
			return getTopViewController(base: selected)

		} else if let presented = base?.presentedViewController {
			return getTopViewController(base: presented)
		}
		return base
	}
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }

}

extension UIView {
	private class func _makeFromNib<T: UIView>() -> T {
		let nibName = NSStringFromClass(T.self).components(separatedBy: ".").last ?? ""
		let bundle = Bundle(for: T.self)
		let nib = UINib(nibName: nibName, bundle: bundle)
		let view = nib.instantiate(withOwner: T.self, options: nil)[0]
		return view as! T
	}

	class func makeFromNib() -> Self {
		return _makeFromNib()
	}
}
