//
//  APICallSettings.swift
//  Picdies
//
//  Created by Thomas Gallier on 23/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import Alamofire

enum APIRequest {
	
	enum APIkeys : String {
		//		case base_url = "http://192.168.1.33:8080/"
		case base_url = "http://picdies-api.herokuapp.com/"
		
	}
	
	enum APIKeysBody:String, DefaultsSerializable {
		case like
		case view
		case share
		case name
		case type
		case user
		case video
		case picture
		case ids
		
		case tags
		case articles
		
		case community
		case targetCommunity
		
		case brand
		case price
		case categories
	}
	
	case feeds
	
	case feedsWithId([APIKeysBody:Any])
	
	case tagsMostPopular
	
	case feed(String)
	case tag(String)
	case user(String)
	case article(String)
	case interaction(String)
	case comment(String)
	
	case updateFeed(String, [APIKeysBody:Any])
	case updateTag(String, [APIKeysBody:Any])
	case updateUser(String, [APIKeysBody:Any])
	case updateArticle(String, [APIKeysBody:Any])
	
	case createUser([String:Any])
	case createFeed([APIKeysBody:Any])
	case createTag([APIKeysBody:Any])
	case createArticle([APIKeysBody:Any])
	
	case updateFeedLike(String, [APIKeysBody:Any])
	case updateFeedShare(String, [APIKeysBody:Any])
	case updateFeedView(String, [APIKeysBody:Any])
	case updateFeedComment(String, [APIKeysBody:Any])
	
	case checkIfUsernameExist(String)
	
	case followUser(String, [APIKeysBody:Any])
	case unfollowUser(String, [APIKeysBody:Any])
	
	func name() -> String {
		switch self {
			case .feeds:
				return "feeds"
			case .tagsMostPopular:
				return "tags/popular"
			case .feedsWithId:
				return "feeds/ids"
			case let .feed(id):
				return "feed/\(id)"
			case let .article(id):
				return "article/\(id)"
			case let .tag(id):
				return "tag/\(id)"
			case let .user(id), let .updateUser(id, _):
				return "user/\(id)"
				
			case let .interaction(id):
				return "feed/interaction/\(id)"
			case let .comment(id):
				return "feed/comment/\(id)"
				
			case let .updateFeed(id, _):
				return "feed/\(id)"
			case let .updateTag(id, _):
				return "tag/\(id)"
			case let .updateArticle(id, _):
				return "article/\(id)"
			case let .updateFeedLike(id, _):
				return "feed/interaction/\(id)/like"
			case let .updateFeedShare(id, _):
				return "feed/interaction/\(id)/share"
			case let .updateFeedView(id, _):
				return "feed/interaction/\(id)/view"
			case let .updateFeedComment(id, _):
				return "article/\(id)"
				
				
			case .createUser:
				return "user"
			case .createFeed:
				return "feed"
			case .createTag:
				return "tag"
			case .createArticle:
				return "article"
				
			case let .checkIfUsernameExist(name):
				return "user/exist/\(name)"
			case let .followUser(id, _):
				return "user/community/follow/\(id)"
			case let .unfollowUser(id, _):
				return "user/community/unfollow/\(id)"
		}
	}
	
	func type() -> HTTPMethod {
		switch self {
			case .feeds, .tagsMostPopular, .feed, .feedsWithId, .article, .tag, .user, .comment, .interaction, .checkIfUsernameExist: return .get
			case .updateFeed, .updateTag, .updateUser, .updateArticle, .updateFeedComment, .updateFeedLike, .updateFeedShare, .updateFeedView, .followUser, .unfollowUser: return .put
			case .createUser, .createFeed, .createTag, .createArticle: return .post
		}
	}
	
	func contentType() -> HTTPHeaders? {
		switch self {
			case .createFeed, .createTag, .createArticle:
				return HTTPHeaders.init(["Content-Type" : "multipart/form-data"])
			default: return nil
		}
	}
	
	func responseType() -> BaseObject.Type? {
		switch self {
			case .feed, .feeds, .feedsWithId, .createFeed, .updateFeed: return Feed.self
			case .user, .updateUser, .createUser, .followUser, .unfollowUser : return User.self
			case .article, .updateArticle, .createArticle: return Article.self
			case .tag, .updateTag, .tagsMostPopular, .createTag: return Tag.self
			case .interaction, .updateFeedLike, .updateFeedShare, .updateFeedView: return FeedInteraction.self
			case .comment, .updateFeedComment: return FeedComment.self
			default: return nil
		}
	}
	
	func body() -> [String:Any]? {
		switch self {
			case
				let .feedsWithId(body),
				let .updateArticle(_, body),
				let .updateUser(_, body),
				let .updateFeed(_, body),
				let .updateTag(_, body),
				let .updateFeedLike(_, body),
				let .updateFeedShare(_, body),
				let .updateFeedView(_, body),
				let .updateFeedComment(_, body),
				let .followUser(_, body),
				let .unfollowUser(_, body),
				let .createTag(body),
				let .createArticle(body),
				let .createFeed(body)
			: return transformeBodyKey(body) as [String : Any]
			case
				let .createUser(body)
			: return body
			default: return nil
		}
	}
	
	func transformeBodyKey(_ body:[APIKeysBody:Any]) -> [String:Any?] {
		var array = [String:Any]()
		for (k,v) in body {
			array[k.rawValue] = v
		}
		return array
	}
	
	
	
	func url() -> String { return "\(APIkeys.base_url.rawValue)\(self.name())" }
	
}

public enum responseContent:String {
	case code
	case message
}
