//
//  AccountManager.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class AccountManager:NSObject, APIFetcherDelegate, ASAuthorizationControllerDelegate {

	static let shared = AccountManager()
	private override init() {
		super.init()
	}

	var appUser:User?

	enum logType:String { case email, facebook, apple, google }
	enum valuesType:String { case email, password, delegate }
	private var currentAuthCompletion: ((Swift.Result<Firebase.User, Error>) -> Void)?

	func register(with type: logType, and values: [valuesType:Any], completion : @escaping (Error?) -> Void) {
		switch type {
			case .email:
				if let email = values[valuesType.email] as? String {
					if let password = values[valuesType.password] as? String {
						UserStorage.newUser[UserStorage.newUserKeys.email.rawValue] = email
						UserStorage.newUser[UserStorage.newUserKeys.password.rawValue] = password
						UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = AccountManager.logType.email.rawValue
						self.registerWithEmail(email: email, password: password) { result in
							self.switchRegisterResult(result: result, completion: completion)
						}
					} else { completion(errorType.noPassword) }
				} else { completion(errorType.noEmail) }
			case .apple:
				if let delegate = values[valuesType.delegate] as? ASAuthorizationControllerPresentationContextProviding {
					UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = AccountManager.logType.apple.rawValue
					self.signInWithApple(delegate: delegate) { result in
						self.switchRegisterResult(result: result, completion: completion)
					}
				} else {
					completion(errorType.errorApple)
				}
			case .facebook:
				if let topVC = UIApplication.getTopViewController() {
					UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = AccountManager.logType.facebook.rawValue
					self.signInWithFacebook(controller: topVC) { result in
						self.switchRegisterResult(result: result, completion: completion)
					}
				} else {
					completion(errorType.errorFacebook)
				}
			case .google:
				if let topVC = UIApplication.getTopViewController() {
					UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = AccountManager.logType.google.rawValue
					self.signInWithGoogle(controller: topVC) { result in
						self.switchRegisterResult(result: result, completion: completion)
					}
				} else {
					completion(errorType.errorGoogle)
				}
		}
	}

	func login(with type: logType, and values: [valuesType:Any], completion : @escaping (Error?) -> Void) {
		switch type {
			case .email:
				if let email = values[valuesType.email] as? String {
					if let password = values[valuesType.password] as? String {
						self.loginWithEmail(email: email, password: password) { result in
							switch result {
								case .success(let user):
									self.connectUser(user, completion: completion)
								case .failure(let error):
									completion(error)
							}
						}
					} else { completion(errorType.noPassword) }
				} else { completion(errorType.noEmail) }
			case .apple:
				if let delegate = values[valuesType.delegate] as? ASAuthorizationControllerPresentationContextProviding {
					UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = AccountManager.logType.apple.rawValue
					self.signInWithApple(delegate: delegate) { result in
						self.switchRegisterResult(result: result, completion: completion)
					}
				} else {
					completion(errorType.errorApple)
				}
			case .facebook:
				if let topVC = UIApplication.getTopViewController() {
					self.signInWithFacebook(controller: topVC) { result in
						switch result {
							case .success(let user):
								self.connectUser(user, completion: completion)
							case .failure(let error):
								completion(error)
						}
					}
				} else {
					completion(errorType.errorFacebook)
				}

			case .google:
				if let topVC = UIApplication.getTopViewController() {
					self.signInWithGoogle(controller: topVC) { result in
						switch result {
							case .success(let user):
								self.connectUser(user, completion: completion)
							case .failure(let error):
								completion(error)
						}
					}
				} else {
					completion(errorType.errorGoogle)
				}

		}
	}

	func switchRegisterResult(result : Swift.Result<Firebase.User, Error>, completion : @escaping (Error?) -> Void) {
		switch result {
			case .success(let firebaseUser):
				UserStorage.newUser[UserStorage.newUserKeys._id.rawValue] = firebaseUser.uid
				UserStorage.newUser[UserStorage.newUserKeys.email.rawValue] = firebaseUser.email
				APIFetcher.shared.request(.createUser(UserStorage.newUser), delegate: self) { (response) in
					if let error = response {
						try? Auth.auth().signOut()
						completion(error)
					} else {
						UserStorage.newUser = [:]
						self.connectUser(firebaseUser, completion: completion)
					}
				}
			case .failure(let error):
				try? Auth.auth().signOut()
				completion(error)
		}
	}

	private func registerWithEmail(email: String, password: String, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
			if let error = error {
				completion(.failure(error))
			} else {
				completion(.success(authResult!.user))
			}
		}
	}

	private func loginWithEmail(email: String, password: String, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
			if let error = error {
				completion(.failure(error))
			} else {
				completion(.success(authResult!.user))
			}
		}
	}


	// MARK: Google

	private func signInWith(credential: AuthCredential, isMFAEnabled: Bool, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		Auth.auth().signIn(with: credential) { (authResult, error) in
			if let e = error {
				print("Auth: SignIn Error: \(e)")
				let authError = e as NSError
				if isMFAEnabled && authError.code == AuthErrorCode.secondFactorRequired.rawValue {
					let resolver = authError.userInfo[AuthErrorUserInfoMultiFactorResolverKey] as! MultiFactorResolver
					var hints = [String]()
					for tmpFactorInfo in resolver.hints {
						if let hint = tmpFactorInfo.displayName { hints.append(hint) }
					}
					self.displayMultipleFactorAuth(with: hints) { result in
						if let name = result {
							var selectedHint: PhoneMultiFactorInfo?
							for tmpFactorInfo in resolver.hints {
								if name == tmpFactorInfo.displayName {
									selectedHint = tmpFactorInfo as? PhoneMultiFactorInfo
									break
								}
							}
							if let s = selectedHint {
								PhoneAuthProvider.provider().verifyPhoneNumber(with: s, uiDelegate: nil, multiFactorSession: resolver.session) { (verificationId, _) in
									if let e2 = error {
										print("Auth: SignIn Error: \(e2)")
										completion(.failure(errorType.noDetail))
									} else {
										self.displayCodeVerification(with: "Code de verification pour \(String(describing: s.displayName))") { (code) in
											if let c = code {
												let cred = PhoneAuthProvider.provider().credential(withVerificationID: verificationId!, verificationCode: c)
												let assertion = PhoneMultiFactorGenerator.assertion(with: cred)
												resolver.resolveSignIn(with: assertion) { (user, resolveError) in
													if let e3 = resolveError {
														print("Auth: Resolve Error: \(e3)")
														completion(.failure(errorType.noDetail))
													} else {
														if let user = user?.user {
															completion(.success(user))
														}
													}
												}
											} else { completion(.failure(errorType.noDetail)) }
										}
									}
								}
							} else { completion(.failure(errorType.noDetail)) }
						} else { completion(.failure(errorType.noDetail)) }
					}
				}
				completion(.failure(errorType.noDetail))
				return
			} else if let r = authResult { completion(.success(r.user)) }
		}
	}


	private func displayMultipleFactorAuth(with hints: [String], completion: (String?) -> Void) { }
	private func displayCodeVerification(with message: String, completion: (String?) -> Void) { }

	private func signInWithGoogle(controller: UIViewController, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		self.currentAuthCompletion = completion
		GIDSignIn.sharedInstance()?.presentingViewController = controller
		GIDSignIn.sharedInstance().signIn()
	}

	func handleGoogleLogin(_ credential: AuthCredential?) {
		if let completion = self.currentAuthCompletion {
			self.currentAuthCompletion = nil
			if let c = credential {
				self.signInWith(credential: c, isMFAEnabled: true, completion: completion)
			} else { completion(.failure(errorType.errorGoogle)) }
		}
	}

	// MARK: Facebook

	private func signInWithFacebook(controller: UIViewController, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		let loginManager = LoginManager()
		loginManager.logIn(permissions: ["email"], from: controller) { (_, error) in
			if let error = error {
				completion(.failure(error))
			} else {
				if let fbTokenSting = AccessToken.current?.tokenString {
					self.signInWith(credential: FacebookAuthProvider.credential(withAccessToken: fbTokenSting), isMFAEnabled: false, completion: completion)
				} else { completion(.failure(errorType.errorFacebook)) }
			}
		}
	}

	// MARK: Apple

	private var currentAppleNonce: String?

	private func signInWithApple(delegate: ASAuthorizationControllerPresentationContextProviding, completion : @escaping (Swift.Result<Firebase.User, Error>) -> Void) {
		let nonce = String.randomNonce()
		self.currentAppleNonce = nonce
		self.currentAuthCompletion = completion
		let appleIDProvider = ASAuthorizationAppleIDProvider()
		let request = appleIDProvider.createRequest()
		request.nonce = nonce.sha256()
		request.requestedScopes = [.email]
		
		let authorizationController = ASAuthorizationController(authorizationRequests: [request] as? [ASAuthorizationRequest] ?? [])
		authorizationController.delegate = self
		authorizationController.presentationContextProvider = delegate
		authorizationController.performRequests()
	}

	func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
		if let currentNonce = self.currentAppleNonce, let completion = self.currentAuthCompletion {
			self.currentAppleNonce = nil
			self.currentAuthCompletion = nil
			if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
				guard let appleIDToken = appleIDCredential.identityToken else {
					print("Unable to fetch identity token")
					completion(.failure(errorType.errorApple))
					return
				}
				guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
					print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
					completion(.failure(errorType.errorApple))
					return
				}
				let credentials = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: currentNonce)
				self.signInWith(credential: credentials, isMFAEnabled: false, completion: completion)
			} else { completion(.failure(errorType.errorApple)) }
		}
	}

	func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
		print(error)
		self.currentAuthCompletion?(.failure(errorType.errorApple))
		self.currentAppleNonce = nil
		self.currentAuthCompletion = nil
	}

	// MARK: Global Login

	func connectUser(_ user : Firebase.User, completion : @escaping (Error?) -> Void) {
		APIFetcher.shared.request(.user(user.uid), delegate: self) { (error) in
			self.appUser = APIFetcher.shared.getCachedObject(ofType: User.self, withId: user.uid) as? User
			if self.appUser != nil { completion(nil) }
			else { completion(errorType.errorLogin) }
		}
	}

	func returnCacheRequest(completion: Error?) { if let error = completion { GlobalManager.shared.error(error: error) } }

	func logout() {
		try? Auth.auth().signOut()
		if Auth.auth().currentUser == nil {
			self.appUser = nil
			UIApplication.shared.windows.first?.rootViewController = UIStoryboard(name: "OnBoarding", bundle: Bundle.main).instantiateInitialViewController()
		}
	}



}
