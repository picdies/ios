//
//  UserDefault.swift
//  Picdies
//
//  Created by Thomas Gallier on 23/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit

// --------- KEY ----- //

extension Key {
	static let likedFeed:Key = "likedFeed"
	static let followUser:Key = "followUser"
	static let followingUser:Key = "followingUser"
	
	static let newUser:Key = "newUser"
}

struct UserStorage {
	@UserDefault(.likedFeed, defaultValue: [String]())
	static var likedFeed:[String]
	
	@UserDefault(.followUser, defaultValue: [String]())
	static var followUser:[String]
	
	@UserDefault(.followingUser, defaultValue: [String]())
	static var followingUser:[String]

	@UserDefault(.newUser, defaultValue: [String:String]())
	static var newUser:[String:String]

	enum newUserKeys:String {
		case _id
		case name
		case first_name
		case last_name
		case birthday
		case email
		case sign_up_with
		case password
	}

}


 // --------- WRAPPER ------- //


struct Key: RawRepresentable, Equatable {
	let rawValue: String
}

extension Key: ExpressibleByStringLiteral {
	init(stringLiteral: String) {
		rawValue = stringLiteral
	}
}

// User Default Property Wrapper
// The marker protocol
protocol DefaultsSerializable {}

extension Data: DefaultsSerializable {}
extension String: DefaultsSerializable {}
extension Date: DefaultsSerializable {}
extension Bool: DefaultsSerializable {}
extension Int: DefaultsSerializable {}
extension Double: DefaultsSerializable {}
extension Float: DefaultsSerializable {}

extension Array: DefaultsSerializable where Element: DefaultsSerializable {}
extension Dictionary: DefaultsSerializable where Key == String, Value: DefaultsSerializable {}

@propertyWrapper
struct UserDefault<T> where T: DefaultsSerializable {
	let key: Key
	let defaultValue: T

	init(_ key: Key, defaultValue: T) {
		self.key = key
		self.defaultValue = defaultValue
	}

	var wrappedValue: T {
		get {
			switch key {
				case .newUser:
					return UserDefaults.standard.object(forKey: key.rawValue) as? T ?? defaultValue
				default:
					return UserDefaults.standard.object(forKey: key.rawValue + "-/-" + AccountManager.shared.appUser!.id) as? T ?? defaultValue
			}
		}
		set {
			switch key {
				case .newUser:
					UserDefaults.standard.set(newValue, forKey: key.rawValue)
				default:
					UserDefaults.standard.set(newValue, forKey: key.rawValue + "-/-" + AccountManager.shared.appUser!.id)
			}
		}
	}

	
}
