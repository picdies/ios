//
//  Sharing.swift
//  Picdies
//
//  Created by Thomas Gallier on 24/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import LinkPresentation

class Sharing : NSObject, UIActivityItemSource {

	static let shared = Sharing()
	private override init() {
		super.init()
	}

	func openSharing(title : String, description : String?, url : URL?) -> UIActivityViewController{
		let linkMetadata = LPLinkMetadata()
		linkMetadata.iconProvider = NSItemProvider(object: UIImage(named: "picdies-logo")!)
		linkMetadata.title = title
		if let u = url { linkMetadata.url = u }
		if let d = description { linkMetadata.originalURL = URL(fileURLWithPath: d) }
		self.metadata = linkMetadata
		let activityVc = UIActivityViewController(activityItems: [self], applicationActivities: nil)
		activityVc.isModalInPresentation = true
		activityVc.popoverPresentationController?.sourceView = UIApplication.shared.windows.first?.rootViewController?.view.subviews.last
		return activityVc
	}

	var metadata: LPLinkMetadata?

	func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
		return "Loading"
	}

	func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
		return self.metadata?.url
	}

	func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
		return self.metadata
	}
}
