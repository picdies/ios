//
//  APICall.swift
//  Picdies
//
//  Created by Thomas Gallier on 23/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON
import Alamofire

class Model:NSObject {
	static let sharedManager: Session = {
		let configuration = URLSessionConfiguration.af.default
		configuration.timeoutIntervalForRequest = 10
		configuration.timeoutIntervalForResource = 10
		configuration.requestCachePolicy = .reloadRevalidatingCacheData
		configuration.waitsForConnectivity = true
		return Session(configuration : configuration)
	}()
	
	private override init() {
		super.init()
	}
}


class APICall:NSObject{
	
	static let shared = APICall()
	private override init() {
		super.init()
	}
	
	func loadFromRequest(_ url : APIRequest, completion : @escaping (Swift.Result<Data, Error>) -> Void) {
		print(url.url())
		Model.sharedManager.request(url.url(), method: url.type(), parameters: url.body(), encoding: JSONEncoding.default, headers: url.contentType()).validate().responseData { response in
			switch response.result {
				case .success(let data):
					completion(.success(data))
				case .failure(let error):
					completion(.failure(errorCode(code: error.responseCode ?? 200)))
			}
		}
	}
	
	func uploadFromRequest(_ url : APIRequest, completion : @escaping (Swift.Result<Data, Error>) -> Void) {
		Model.sharedManager.upload(multipartFormData: { (multipartFormData) in
			let body = url.body()
			if let name = body?[APIRequest.APIKeysBody.name.rawValue] as? String {
				multipartFormData.append(name.data(using: .utf8)!, withName: "name")
			}
			if let type = body?[APIRequest.APIKeysBody.type.rawValue] as? String {
				multipartFormData.append(type.data(using: .utf8)!, withName: "type")
			}
			if let user = body?[APIRequest.APIKeysBody.user.rawValue] as? String {
				multipartFormData.append(user.data(using: .utf8)!, withName: "user")
			}
			if let brand = body?[APIRequest.APIKeysBody.brand.rawValue] as? String {
				multipartFormData.append(brand.data(using: .utf8)!, withName: "brand")
			}
			if let price = body?[APIRequest.APIKeysBody.price.rawValue] as? String {
				multipartFormData.append(price.data(using: .utf8)!, withName: "price")
			}
			if let video = body?[APIRequest.APIKeysBody.video.rawValue] as? URL {
				multipartFormData.append(video, withName: "video")
			}
			if let image = body?[APIRequest.APIKeysBody.picture.rawValue] as? URL {
				multipartFormData.append(image, withName: "picture", fileName: String.random(),  mimeType: "image/png")
			}
			if let images = body?[APIRequest.APIKeysBody.picture.rawValue] as? [URL] {
				var s = [String]()
				images.forEach { (url) in
					let r = String.random()
					s.append(r)
					multipartFormData.append(url, withName: r, fileName: r, mimeType: "image/png")
				}
				multipartFormData.append(s.joined(separator: ",").data(using: .utf8)!, withName: "pictures")
			}
			
			if let tags = body?[APIRequest.APIKeysBody.tags.rawValue] as? [String] {
				multipartFormData.append(tags.joined(separator: ",").data(using: .utf8)!, withName: "tags")
			}
			
			if let articles = body?[APIRequest.APIKeysBody.articles.rawValue] as? [String] {
				multipartFormData.append(articles.joined(separator: ",").data(using: .utf8)!, withName: "articles")
			}
			
			if let categories = body?[APIRequest.APIKeysBody.categories.rawValue] as? [String] {
				multipartFormData.append(categories.joined(separator: ",").data(using: .utf8)!, withName: "categories")
			}
			
		},to: url.url(), method: url.type(), headers: url.contentType()
		).responseData { response in
			switch response.result {
				case .success(let data):
					completion(.success(data))
				case .failure(let error):
					print(error.errorDescription)
					completion(.failure(errorCode(code: error.responseCode ?? 200)))
			}
		}
	}
}

