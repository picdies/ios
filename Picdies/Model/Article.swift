//
//  Article.swift
//  Picdies
//
//  Created by Thomas Gallier on 22/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation

class Article:BaseObject  {
	
	enum keys:String {
		case brand, price, ressources, categories, feeds, dressings
	}
	
	var brand:String?
	var price:String?
	
	var ressources = [Ressource]()
	var feeds = [Feed]()
	var categories = [Category]()
	var dressings = [Dressing]()
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.brand.rawValue] as? String { self.brand = s }
		if let s = dict[keys.price.rawValue] as? Double { self.price = self.formatNumber(s)?.replacingOccurrences(of: ",", with: ".") }
		
		if let s = dict[keys.ressources.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addRessource(dict: dict)
			}
		}
		
		if let s = dict[keys.feeds.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addFeed(dict: dict)
			}
		}
		
		if let s = dict[keys.categories.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addCategory(dict: dict)
			}
		}
		
		if let s = dict[keys.dressings.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addDressing(dict: dict)
			}
		}
	}
	
	func formatNumber(_ number: Double) -> String? {
		
		let formatter = NumberFormatter()
		formatter.minimumFractionDigits = 2
		formatter.maximumFractionDigits = 2
		formatter.minimumIntegerDigits = 1
		
		let formattedNumber = formatter.string(from: NSNumber.init(value: number))
		
		return formattedNumber
		
	}
	
	
	func addRessource(dict : [String:Any]) {
		self.ressources.append(Ressource(dict: dict))
	}
	
	func addCategory(dict : [String:Any]) {
		self.categories.append(Category(dict: dict))
	}
	
	func addFeed(dict : [String:Any]) {
		self.feeds.append(Feed(dict: dict))
	}
	
	func addDressing(dict : [String:Any]) {
		self.dressings.append(Dressing(dict: dict))
	}
	
	
}
