//
//  Feed.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import Firebase
import AVKit
import SwiftDate

class FeedReport:BaseObject {
	enum keys : String { case user, type }
	enum type : String { case report, none }
	
	var user:User?
	var type:FeedReport.type = .none
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.user.rawValue] as? [String:Any] { self.user = User(dict: s) }
		if let s = dict[keys.type.rawValue] as? String { self.type = FeedReport.type.init(rawValue: s) ?? .none }
	}
}

class FeedInteraction:BaseObject {
	enum keys : String { case view, like, comment, share }
	
	var view:Int?
	var like:Int?
	var share:Int?
	var comment:[FeedComment]?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.like.rawValue] as? Int { self.like = s }
		if let s = dict[keys.share.rawValue] as? Int { self.share = s }
		if let s = dict[keys.view.rawValue] as? Int { self.view = s }
		
		if let s = dict[keys.comment.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addComment(dict: dict)
			}
		}
	}
	
	func addComment(dict : [String:Any]) {
		self.comment?.append(FeedComment(dict: dict))
	}
}

class FeedComment:BaseObject {
	enum keys : String { case user, comment, like }
	
	var user:User?
	var comment:String?
	var like:Int?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.user.rawValue] as? [String:Any] { self.user = User(dict: s) }
		if let s = dict[keys.comment.rawValue] as? String { self.comment = s }
		if let s = dict[keys.like.rawValue] as? Int { self.like = s }
	}
}

class Feed : BaseObject  {
	
	enum keys : String {
		case ressources, tags, articles, user, interaction, type, verified, reports
	}
	
	enum interactionType : String {
		case like, comment, share
	}
	
	enum feedType: String {
		case picture, video
	}
	
	var type:feedType?
	var user: User?
	var interaction: FeedInteraction?
	
	var ressources = [Ressource]()
	var reports = [FeedReport]()
	var tags = [Tag]()
	var articles = [Article]()
	
	var verified:Bool = false
	
	var stringDate:String?
	var asset:AVURLAsset?
	
	required init(dict: [String: Any]) { super.init(dict: dict) }
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		
		// TMP MODIF ADD CONTENT --
		self.ressources.removeAll()
		self.tags.removeAll()
		self.articles.removeAll()
		self.reports.removeAll()
		
		// ----
		if let s = dict[keys.type.rawValue] as? String { self.type = feedType.init(rawValue: s) }
		if let s = dict[keys.ressources.rawValue] as? [[String:Any]] { s.forEach { (dict) in self.addRessource(dict: dict) } }
		if let s = dict[keys.user.rawValue] as? [String:Any] { self.user = User(dict: s) }
		if let s = dict[keys.tags.rawValue] as? [[String:Any]] { s.forEach { (dict) in self.addTag(dict: dict) } }
		if let s = dict[keys.articles.rawValue] as? [[String:Any]] { s.forEach { (dict) in self.addArticle(dict: dict) } }
		if let s = dict[keys.interaction.rawValue] as? [String:Any] { self.interaction = FeedInteraction(dict: s) }
		self.getIntervalDate { (date) in
			self.stringDate = date
		}
	}
	
	func addRessource(dict : [String:Any]) {
		self.ressources.append(Ressource(dict: dict))
	}
	
	func addTag(dict : [String:Any]) {
		self.tags.append(Tag(dict: dict))
	}
	
	func addArticle(dict : [String:Any]) {
		self.articles.append(Article(dict: dict))
	}
	
	// MARK: Interval Date between current date and feed created date
	
	func getIntervalDate(completion:@escaping (String) -> Void) {
		DispatchQueue.main.async {
			let dateFeed = DateInRegion(self.created_at ?? "", format: .none, region: .local)
			let currentDate = DateInRegion(Date().toString(), format: .none, region: .local)
			let year = dateFeed?.getInterval(toDate: currentDate, component: .year) ?? 0
			let day = dateFeed?.getInterval(toDate: currentDate, component: .day) ?? 0
			let hour = dateFeed?.getInterval(toDate: currentDate, component: .hour) ?? 0
			let minute = dateFeed?.getInterval(toDate: currentDate, component: .minute) ?? 0
			let second = dateFeed?.getInterval(toDate: currentDate, component: .second) ?? 0
			
			if year > 0 { completion( (dateFeed?.toFormat("dd MMMM yyyy", locale: Locale.preferredLocale()))! ) }
			else if day > 6 { completion( (dateFeed?.toFormat("dd MMMM", locale: Locale.preferredLocale()))! ) }
			else if day > 0 { completion( "Il y a " + String(day) + (day > 1 ? " jours" : " jour") ) }
			else if hour > 0 { completion( "Il y a " + String(hour) + (hour > 1 ? " heures" : " heure") ) }
			else if minute > 0 { completion( "Il y a " + String(minute) + (minute > 1 ? " minutes" : " minute") ) }
			else if second > 0 { completion( "Il y a " + String(second) + (second > 1 ? " secondes" : " seconde") ) }
			else { completion("un moment") }
		}
	}
}

class NewFeed {
	enum keys : String {
		case feed_name, feed_type, user, new_ressources, new_tags, tags, new_articles, articles
	}
	
	var feed_name:String?
	var feed_type:Feed.feedType?
	var user:User?
	var new_ressources = [NewRessource]()
	var new_tags = [NewTag]()
	var new_articles = [NewArticle]()
	var tags = [Tag]()
	var articles = [Article]()
	
	init() {}
}

class NewRessource {
	enum keys : String {
		case type, data
	}
	
	var type:Feed.feedType?
	var data:Data?
	
	init(type: Feed.feedType, data: Data) {
		self.data = data
		self.type = type
	}
	
}

class NewTag {
	enum keys : String {
		case id, name
	}
	
	var id:String?
	var name:String?
	
	init(name: String) {
		self.name = name
	}
	
	init(id: String) {
		self.id = id
	}
	
}

class NewArticle {
	enum keys : String {
		case id, name, brand, price, new_ressources, categories, new_categories
	}
	
	var id:String?
	var name:String?
	var brand:String?
	var price:String?
	var new_ressources = [NewRessource]()
	var new_categories = [NewCategory]()
	var categories = [Category]()
	
	init(name: String, brand: String, price: String, new_ressources : [NewRessource], new_categories : [NewCategory], categories : [Category]) {
		self.name = name
	}
	
	init(id: String) {
		self.id = id
	}
	
}

class NewCategory {
	enum keys : String {
		case id, name
	}
	
	var id:String?
	var name:String?
	
	init(name: String) {
		self.name = name
	}
	
	init(id: String) {
		self.id = id
	}
	
}


