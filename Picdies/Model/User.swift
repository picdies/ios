//
//  User.swift
//  Picdies
//
//  Created by Thomas Gallier on 22/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import SwiftDate


class UserFeed:BaseObject {
	enum keys:String {
		case feed, type
	}
	
	enum user_feed_type:String {
		case owner, like, fave
	}
	
	var feed_id:String?
	var feed:Feed?
	var type:user_feed_type?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		
		if let s = dict[keys.feed.rawValue] as? String { self.feed_id = s }
		if let s = dict[keys.feed.rawValue] as? [String:Any] {
			self.feed = Feed(dict : s)
			self.feed_id = self.feed?.id
		}
		if let s = dict[keys.type.rawValue] as? String { self.type = user_feed_type.init(rawValue: s) }
	}
}

class UserDressing:BaseObject {
	enum keys:String {
		case dressing, type
	}
	
	enum user_dressing_type:String {
		case owner, fave
	}
	
	var dressing_id:String?
	var dressing:Dressing?
	var type:user_dressing_type?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.dressing.rawValue] as? String { self.dressing_id = s }
		if let s = dict[keys.dressing.rawValue] as? [String:Any] {
			self.dressing = Dressing(dict: s)
			self.dressing_id = self.dressing?.id
		}
		if let s = dict[keys.type.rawValue] as? String { self.type = user_dressing_type.init(rawValue: s) }
	}
}

class UserArticle:BaseObject {
	enum keys:String {
		case article, type
	}
	
	enum user_article_type:String {
		case buy, fave
	}
	
	var article_id:String?
	var article:Article?
	var type:user_article_type?
	var date:Int?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.article.rawValue] as? String { self.article_id = s }
		if let s = dict[keys.article.rawValue] as? [String:Any] {
			self.article = Article(dict : s)
			self.article_id = self.article?.id
		}
		if let s = dict[keys.type.rawValue] as? String { self.type = user_article_type.init(rawValue: s) }
	}
}

class UserCommunity:BaseObject {
	enum keys:String {
		case user, type
	}
	
	enum user_community_type:String {
		case follow,following,blocked
	}
	
	var user_id: String?
	var user:User?
	var type:user_community_type?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.user.rawValue] as? String { self.user_id = s }
		if let s = dict[keys.user.rawValue] as? [String:Any] {
			self.user = User(dict : s)
			self.user_id = self.user?.id
		}
		if let s = dict[keys.type.rawValue] as? String { self.type = user_community_type.init(rawValue: s) }
	}
}

class UserNetwork:BaseObject{
	enum keys:String {
		case type
	}
	
	enum user_network_type:String {
		case instagram, twitter, snapchat, facebook
	}
	
	var type:user_network_type?
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.type.rawValue] as? String { self.type = user_network_type.init(rawValue: s) }
	}
}


class User:BaseObject  {
	
	enum keys:String {
		case first_name, last_name, description, email, country, password
		case birthday
		case avatar
		case sign_up_with
		case gender
		case notification, isPrivate
		case feeds, dressings, articles, communities, networks
	}
	
	enum gender_type:String { case male, female, noGender = "none" }
	enum sign_up_with_type:String { case email, apple, google, facebook }
	
	var first_name,last_name,description,birthday,email,country:String?
	var avatar:URL?
	
	var feeds = [UserFeed]()
	var dressings = [UserDressing]()
	var articles = [UserArticle]()
	var communities = [UserCommunity]()
	var networks = [UserNetwork]()
	
	var notification:Bool = false
	var isPrivate:Bool = false
	
	var gender:gender_type?
	var sign_up_with:sign_up_with_type?
	
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		
		if let s = dict[keys.first_name.rawValue] as? String { self.first_name = s }
		if let s = dict[keys.last_name.rawValue] as? String { self.last_name = s }
		if let s = dict[keys.birthday.rawValue] as? String { self.birthday = s }
		if let s = dict[keys.email.rawValue] as? String { self.email = s }
		if let s = dict[keys.description.rawValue] as? String { self.description = s }
		if let s = dict[keys.country.rawValue] as? String { self.country = s }
		
		if let s = dict[keys.gender.rawValue] as? String { self.gender = gender_type.init(rawValue: s) }
		if let s = dict[keys.sign_up_with.rawValue] as? String { self.sign_up_with = sign_up_with_type.init(rawValue: s) }
		
		if let s = dict[keys.avatar.rawValue] as? String { self.avatar = URL(string: s) }
		
		if let s = dict[keys.notification.rawValue] as? Bool { self.notification = s }
		if let s = dict[keys.isPrivate.rawValue] as? Bool { self.isPrivate = s }
		
		
		if let s = dict[keys.feeds.rawValue] as? [[String:Any]] {
			self.feeds.removeAll()
			
			s.forEach { (d) in
				self.addUserFeed(dict: d)
			}
		}
		
		if let s = dict[keys.dressings.rawValue] as? [[String:Any]] {
			self.dressings.removeAll()

		
			s.forEach { (d) in
				self.addUserDressing(dict: d)
			}
		}
		
		if let s = dict[keys.articles.rawValue] as? [[String:Any]] {
			self.articles.removeAll()
			
			s.forEach { (d) in
				self.addUserArticle(dict: d)
			}
		}
		
		if let s = dict[keys.communities.rawValue] as? [[String:Any]] {
			self.communities.removeAll()
			
			s.forEach { (d) in
				self.addUserCommunity(dict: d)
			}
			
			
		}
		
		if let s = dict[keys.networks.rawValue] as? [[String:Any]] {
			self.networks.removeAll()
			
			s.forEach { (d) in
				self.addUserNetwork(dict: d)
			}
		}
	}
	
	func addUserFeed(dict : [String:Any]) {
		let u = UserFeed(dict: dict)
		if !self.feeds.contains(where: { (item) -> Bool in
			return item.id == u.id
		}) {
			self.feeds.insert(u, at: 0)
		} else {
			self.feeds.first { (userFeed) -> Bool in
				return userFeed.id == u.id
			}?.setupWith(dict: dict)
		}
	}
	
	func addUserDressing(dict : [String:Any]) {
		let u = UserDressing(dict: dict)
		if !self.dressings.contains(where: { (item) -> Bool in
			return item.id == u.id
		}) {
			self.dressings.insert(u, at: 0)
		} else {
			self.dressings.first { (userDressing) -> Bool in
				return userDressing.id == u.id
			}?.setupWith(dict: dict)
		}
	}
	
	func addUserArticle(dict : [String:Any]) {
		let u = UserArticle(dict: dict)
		if !self.articles.contains(where: { (item) -> Bool in
			return item.id == u.id
		}) {
			self.articles.insert(u, at: 0)
		} else {
			self.articles.first { (userArticle) -> Bool in
				return userArticle.id == u.id
			}?.setupWith(dict: dict)
		}
	}
	
	func addUserNetwork(dict : [String:Any]) {
		let u = UserNetwork(dict: dict)
		if !self.networks.contains(where: { (item) -> Bool in
			return item.id == u.id
		}) {
			self.networks.insert(u, at: 0)
		} else {
			self.networks.first { (userNetwork) -> Bool in
				return userNetwork.id == u.id
			}?.setupWith(dict: dict)
		}
	}
	
	func addUserCommunity(dict : [String:Any]) {
		let u = UserCommunity(dict: dict)
		if !self.communities.contains(where: { (item) -> Bool in
			return item.id == u.id
		}) {
			self.communities.insert(u, at: 0)
		} else {
			self.communities.first { (userCommunity) -> Bool in
				return userCommunity.id == u.id
			}?.setupWith(dict: dict)
		}
	}
	
	// MARK: Interval Date between current date and user created date
	
	func getStringDateCreatedAt() -> String {
		let dateUser = DateInRegion(self.created_at ?? "", format: .none, region: .local)
		return (dateUser?.toFormat("dd MMMM yyyy", locale: Locale.preferredLocale()))!
	}
	
	// MARK: Function utils
	
	func getAvatar() -> UIImage {
		switch self.gender {
			case .female:
				return UIImage(named: "avatar/girl")!
			case .male:
				return UIImage(named: "avatar/man")!
			default:
				return UIImage(named: "avatar/none")!
		}
	}
	
	func checkIfFollowFollowingBlockedUser(ofType type : UserCommunity.user_community_type, andId id : String) -> Bool {
		return self.communities.filter { (community) -> Bool in
			return community.type == type && community.user_id == id
		}.count > 0
	}
	
}
