//
//  Dressing.swift
//  Picdies
//
//  Created by Thomas Gallier on 19/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation

class Dressing:BaseObject  {
	
	enum keys:String {
		case articles, users
	}
	
	var articles = [Article]()
	var users = [User]()
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.articles.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addArticle(dict: dict)
			}
		}
		if let s = dict[keys.users.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addUser(dict: dict)
			}
		}
		
	}
	
	func addUser(dict : [String:Any]) {
		self.users.append(User(dict: dict))
	}
	
	func addArticle(dict : [String:Any]) {
		self.articles.append(Article(dict: dict))
	}
}
