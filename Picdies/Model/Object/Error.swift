//
//  Error.swift
//  Picdies
//
//  Created by Thomas Gallier on 27/01/2021.
//

import Foundation
import UIKit
import Firebase

public enum errorType:Error {
	case noDataFound
	case noEmail
	case noPassword
	case noFirstName
	case noLastName
	case noBirthday
	case noUsername
	
	case uniqueUsername
	case specialCharacters
	case uniqueEmail
	
	case errorApple
	case errorGoogle
	case errorFacebook
	case errorLogin
	
	case noThumbnail
	case noVideo
	case noPicture
	
	case noDetail
}


public func errorCode(code: Int) -> errorType {
	switch code {
		case 550: return .uniqueUsername // Le nom d'utilisateur est déjà utilisé
		case 551: return .uniqueEmail // L'e-mail est déjà utilisé
		default: return .noDetail
	}
}

extension errorType: LocalizedError {
	public var errorDescription: String? {
		switch self {
			case .noDataFound: return NSLocalizedString("Aucune donnée trouvée", comment: "No data")
			case .noEmail: return NSLocalizedString("Aucun e-mail fourni", comment: "No Email")
			case .noPassword: return NSLocalizedString("Aucun mot de passe fourni" , comment: "No password")
			case .noFirstName: return NSLocalizedString("Veuillez saisir votre prénom" , comment: "No firstname")
			case .noLastName: return NSLocalizedString("Veuillez saisir votre nom" , comment: "No lastname")
			case .noBirthday: return NSLocalizedString("Veuillez sélectionner votre date de naissance" , comment: "No birthday")
			case .noUsername: return NSLocalizedString("Veuillez saisir votre nom d'utilisateur" , comment: "No username")
				
			case .uniqueUsername: return NSLocalizedString("Ce nom d'utilisateur est déjà utilisé" , comment: "Unique username")
			case .uniqueEmail: return NSLocalizedString("Cet email est déjà utilisé" , comment: "Unique email")
			case .specialCharacters: return NSLocalizedString("Caractères invalides" , comment: "invalidCharacteres")
			case .noDetail: return NSLocalizedString("Une erreur est survenue" , comment: "Error")
				
			case .errorApple: return NSLocalizedString("Une erreur est survenue avec Apple" , comment: "Apple")
			case .errorGoogle: return NSLocalizedString("Une erreur est survenue avec Google" , comment: "Google")
			case .errorFacebook: return NSLocalizedString("Une erreur est survenue avec Facebook" , comment: "Facebook")
				
			case .noThumbnail: return NSLocalizedString("Aucune vignette trouvée" , comment: "noThumbnail")
			case .noVideo: return NSLocalizedString("Aucune vidéo trouvée" , comment: "noVideo")
			case .noPicture: return NSLocalizedString("Aucune photo trouvée" , comment: "noPicture")
				
				
			case .errorLogin: return NSLocalizedString("Une erreur est survenue pendant la connexion" , comment: "Login")
		}
	}
}

extension AuthErrorCode {
	var description: String? {
		switch self {
			case .emailAlreadyInUse:
				return "Cet e-mail est déjà utilisé"
			case .userDisabled:
				return "Cet utilisateur est désactivé"
			case .operationNotAllowed:
				return "Cette opération n'est pas autorisée"
			case .invalidEmail:
				return "Cet e-mail est invalide"
			case .wrongPassword:
				return "Ce mot de passe est incorrect"
			case .userNotFound:
				return "Cet utilisateur est inconnu"
			case .networkError:
				return "Problème de connexion au serveur"
			case .weakPassword:
				return "Ce mot de passe est trop faible"
			case .missingEmail:
				return "L'e-mail est nécessaire"
			case .internalError:
				return "Erreur interne"
			case .invalidCustomToken:
				return "Erreur de token"
			case .tooManyRequests:
				return "Trop de requêtes"
			default:
				return nil
		}
	}
}

public extension Error {
	var localizedDescription: String {
		let error = self as NSError
		if error.domain == AuthErrorDomain {
			if let code = AuthErrorCode(rawValue: error.code) {
				if let errorString = code.description {
					return errorString
				}
			}
		}
		return error.localizedDescription
	} }
