//
//  UserViewController.swift
//  Picdies
//
//  Created by Thomas Gallier on 28/01/2021.
//

import Foundation
import UIKit
import SDWebImage
import MXParallaxHeader
import Firebase

extension AccountViewController {
	enum TableKeys:Int, CaseIterable{
		case feed = 0
		case dressing = 1
		
		var cellName: String {
			get {
				switch self {
					case .feed: return "FeedCollectionBigCell"
					case .dressing: return "ArticleCollectionMediumCell"
				}
			}
		}
		
		func swipe(direction: UISwipeGestureRecognizer.Direction) -> TableKeys {
			switch self {
				case .feed:
					return direction == .right ? self : .dressing
				case .dressing:
					return direction == .right ? .feed : self
			}
		}
		
		var heightForRowAt:CGSize {
			switch self {
				case .feed: return CGSize(width: (UIScreen.main.bounds.width / 2) - 1, height: ((UIScreen.main.bounds.width / 2) * 16/9))
				case .dressing: return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
			}
		}
		
		func registerIn(collectionView: UICollectionView) { collectionView.register(UINib(nibName: self.cellName, bundle: nil), forCellWithReuseIdentifier: self.cellName)}
		
	}
}

class AccountViewController:UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, APIFetcherDelegate, FeedCollectionBigCellProtocol, ArticleCollectionMediumProtocol {
	
	@IBOutlet weak var numberOfFollowers: UILabel!
	@IBOutlet weak var numberOfCollections: UILabel!
	@IBOutlet weak var numberOfPublications: UILabel!
	@IBOutlet weak var stackViewSocialMedia: UIStackView!
	@IBOutlet weak var btnSnapchat: UIButton!
	@IBOutlet weak var btnTwitter: UIButton!
	@IBOutlet weak var btnInsta: UIButton!
	@IBOutlet weak var btnFacebook: UIButton!
	@IBOutlet weak var heightScrollView: NSLayoutConstraint!
	@IBOutlet weak var descriptionUser: UILabel!
	@IBOutlet weak var viewCategory: UIView!
	@IBOutlet weak var viewInfoNumber: UIView!
	@IBOutlet weak var viewBtnFollowOrSend: UIView!
	@IBOutlet weak var nameUser: UILabel!
	@IBOutlet weak var avatarUser: UIImageView!
	@IBOutlet weak var btnFollowOrSend: UIButton!
	@IBOutlet weak var stackViewInfoNumber: UIStackView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var optionBtn: UIButton!
	@IBOutlet weak var stackViewCategory: UIStackView!
	@IBOutlet weak var mainScrollView: UIScrollView!
	@IBOutlet weak var mainView: UIView!
	@IBOutlet weak var btnBack: UIButton!
	@IBOutlet weak var viewHeader: UIView!
	
	var isCurrentUser:Bool = false
	var user:User!
	var currentCategory:TableKeys = .feed
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if self.user == nil || self.user.id == AccountManager.shared.appUser?.id {
			self.user = AccountManager.shared.appUser
			self.isCurrentUser = true
			self.btnBack.isHidden = true
			self.btnFollowOrSend.setTitle("Inviter des amis", for: .normal)
			self.optionBtn.setImage(UIImage(systemName: "gearshape"), for: .normal)
		} else {
			self.optionBtn.setImage(UIImage(systemName: "ellipsis"), for: .normal)
			self.btnBack.isHidden = false
		}
		
		self.registerNib()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
		if let u = APIFetcher.shared.getCachedObject(ofType: User.self, withId: self.user.id) as? User{
			self.user = u
		}
		
		self.setup()
		self.updateHeightOfScrollView()
		self.request()
		self.setupSocialMedial()
		
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		self.updateBorderButton()
		self.updateHeightOfScrollView()
	}
	
	// MARK: Setup
	
	func setupImageScrollZoom() {
		self.mainScrollView.parallaxHeader.view = self.viewHeader
		self.mainScrollView.parallaxHeader.height = self.viewHeader.bounds.height
		self.mainScrollView.parallaxHeader.mode = .fill
		self.mainScrollView.parallaxHeader.minimumHeight = 80
	}
	
	func setup() {
		self.setupUI()
		self.setupImageScrollZoom()
		self.setupData()
		self.setupGesture()
	}
	
	func updateButton() {
		if self.user.id != AccountManager.shared.appUser?.id {
			if AccountManager.shared.appUser?.checkIfFollowFollowingBlockedUser(ofType: .follow, andId: self.user.id) ?? false {
				self.btnFollowOrSend.setTitle("Suivi", for: .normal)
			} else {
				self.btnFollowOrSend.setTitle("Suivre", for: .normal)
			}
		}
	}
	
	func setupUI() {
		self.viewHeader.alpha = 1
		
		self.viewCategory.layer.addBorder(side: .bottom, thickness: 0.5, color: UIColor.systemGray5.cgColor)
		self.viewBtnFollowOrSend.layer.addBorder(side: .top, thickness: 0.5, color: UIColor.systemGray5.cgColor)
		
		self.btnFollowOrSend.titleLabel?.numberOfLines = 1
		self.btnFollowOrSend.titleLabel?.adjustsFontSizeToFitWidth = true
		self.btnFollowOrSend.titleLabel?.lineBreakMode = .byClipping
	}
	
	func setupGesture() {
		let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeCategory(_:)))
		swipeRight.direction = .right
		
		let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeCategory(_:)))
		swipeLeft.direction = .left
		
		self.collectionView.addGestureRecognizer(swipeLeft)
		self.collectionView.addGestureRecognizer(swipeRight)
		
		let tapSendInviteOrFollow = UITapGestureRecognizer(target: self, action: self.isCurrentUser ? #selector(self.tapInvite) : #selector(self.tapFollow) )
		tapSendInviteOrFollow.numberOfTouchesRequired = 1
		tapSendInviteOrFollow.numberOfTapsRequired = 1
		self.btnFollowOrSend.addGestureRecognizer(tapSendInviteOrFollow)
		
		let tapOption = UITapGestureRecognizer(target: self, action: self.isCurrentUser ? #selector(self.tapOptionConnectedUser) : #selector(self.tapOptionUser) )
		tapOption.numberOfTapsRequired = 1
		tapOption.numberOfTouchesRequired = 1
		self.optionBtn.addGestureRecognizer(tapOption)
		
	}
	
	func setupData() {
		
		DispatchQueue.main.async {
			if let url = self.user.avatar {
				self.avatarUser.sd_setImage(with: url) {_,_,_,_ in }
			} else {
				self.avatarUser.image = self.user.getAvatar()
			}
		}
		self.nameUser.text = self.user.name
		self.descriptionUser.text = self.user.description ?? ("Inscrit depuis le " + self.user.getStringDateCreatedAt())
		
		self.numberOfPublications.text = String(self.user.feeds.count)
		self.numberOfCollections.text = String(self.user.dressings.count)
		self.numberOfFollowers.text = String(self.user.communities.filter({ $0.type == .following }).count)
		
		self.updateButton()
	}
	
	func updateData() {
		self.collectionView.reloadData()
		self.view.setNeedsLayout()
		self.view.layoutIfNeeded()
	}
	
	
	// - Underline
	var borderBottom: UIView?
	func updateBorderButton() {
		for view in self.stackViewCategory.arrangedSubviews {
			if let button = view as? UIButton {
				if button.tag == self.currentCategory.rawValue {
					if borderBottom == nil {
						borderBottom = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height - 2, width: button.frame.size.width, height: 2))
						borderBottom?.backgroundColor = .label
						stackViewCategory.addSubview(borderBottom!)
					} else {
						UIView.animate(withDuration: 0.2, animations: {
							self.borderBottom?.frame = CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height - 2, width: button.frame.size.width, height: 2)
							self.view.layoutIfNeeded()
						})
					}
					UIView.animate(withDuration: 0.2, animations: {
						button.tintColor = .label
					})
				} else {
					UIView.animate(withDuration: 0.2, animations: {
						button.tintColor = .secondaryLabel
					})
				}
			}
		}
	}
	
	// MARK: - Action Handle
	@IBAction func updateCategory(sender: UIButton) {
		if self.currentCategory.rawValue != (sender.tag) {
			Vibration.soft.vibrate()
			self.currentCategory = TableKeys.init(rawValue: sender.tag)!
			self.updateData()
		}
	}
	
	@objc func swipeCategory(_ sender: UISwipeGestureRecognizer) {
		Vibration.soft.vibrate()
		self.currentCategory = currentCategory.swipe(direction: sender.direction)
		self.updateData()
	}
	
	@objc func tapInvite() {
		Vibration.selection.vibrate()
		let activity = Sharing.shared.openSharing(title: "Je suis sur Picdies, viens découvrir mon profil en téléchargeant l'application !", description: nil, url: nil)
		activity.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) -> Void in
		}
		self.present(activity, animated: true, completion: {})
	}
	
	@objc func tapFollow() {
		Vibration.selection.vibrate()
		if !(AccountManager.shared.appUser?.checkIfFollowFollowingBlockedUser(ofType: .follow, andId: self.user.id) ?? false) {
			APIFetcher.shared.request(.followUser(AccountManager.shared.appUser!.id, [.user: self.user.id]), delegate: self) { (error) in
				if error != nil { return }
				self.user = APIFetcher.shared.getCachedObject(ofType: User.self, withId: self.user.id) as? User
				self.setupData()
			}
		} else {
			APIFetcher.shared.request(.unfollowUser(AccountManager.shared.appUser!.id, [.user: self.user.id, .community: AccountManager.shared.appUser?.communities.first(where: { $0.type == .follow && $0.user_id == self.user.id })?.id, .targetCommunity : self.user?.communities.first(where: { $0.type == .following && $0.user_id == AccountManager.shared.appUser!.id})?.id]), delegate: self) { (error) in
				if error != nil { return }
				self.user = APIFetcher.shared.getCachedObject(ofType: User.self, withId: self.user.id) as? User
				self.setupData()
			}
		}
	}
	
	@objc func tapOptionConnectedUser() {
		AccountManager.shared.logout()
	}
	
	@objc func tapOptionUser() {
		
	}
	
	// MARK: Request
	
	@objc func request() {
		self.collectionView.activityIndicator(show: true)
		APIFetcher.shared.request(.user(self.user.id), delegate: self) { (response) in
			self.responseFetchUser(response: response)
		}
	}
	
	
	func responseFetchUser(response : Error?) {
		if let _ = response {
			return
		} else {
			self.user = APIFetcher.shared.getCachedObject(ofType: User.self, withId: self.user.id) as? User
			self.collectionView?.activityIndicator(show: false)
			self.collectionView?.reloadData()
			self.updateHeightOfScrollView()
			self.setupData()
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseFetchUser(response: completion)
	}
	
	// MARK: Collection View
	
	func registerNib() {
		TableKeys.allCases.forEach { (key) in
			key.registerIn(collectionView: self.collectionView)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch self.currentCategory {
			case .feed:
				return self.user.feeds.count
			case .dressing:
				return self.user.dressings.count
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		switch self.currentCategory {
			case .feed:
				return TableKeys.feed.heightForRowAt
			case .dressing:
				return TableKeys.dressing.heightForRowAt
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		var id = TableKeys.feed.cellName
		switch self.currentCategory {
			case .dressing:
				id = TableKeys.dressing.cellName
			default: break
		}
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)
		
		switch id {
			case TableKeys.feed.cellName:
				if let f = self.user.feeds[indexPath.row].feed {
					(cell as! FeedCollectionBigCell).configure(delegate: self, feed: f)
				}
			case TableKeys.dressing.cellName:
				if self.user.dressings[indexPath.row].dressing != nil { }
			default: break
		}
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		switch self.currentCategory {
			case .feed:
				if let f = self.user.feeds[indexPath.row].feed {
					let viewController = FeedViewController.instantiate(feed: f, andIndex: 0, isPlaying: true) as! FeedViewController
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			case .dressing:
				break
		}
	}
	
	// MARK: Setup Images scroll
	
	@IBAction func backAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if scrollView == self.mainScrollView {
			if scrollView.contentOffset.y < 0 {
				let alpha = (((UIScreen.main.bounds.width + 75) - abs(scrollView.contentOffset.y)) / (UIScreen.main.bounds.width + 75)) * 1.5
				self.stackViewInfoNumber.alpha = 1 - alpha
				self.btnFollowOrSend.alpha = 1 - alpha
			}
		}
	}
	
	// MARK: Delegate feed cell
	
	func tapLike(cell: FeedCollectionBigCell) {
		
	}
	
	// MARK: Delegate article cell
	
	func tapBookmark(cell: ArticleCollectionMediumCell) {
		
	}
	
	// MARK: Calcul height of scroll view
	
	func updateHeightOfScrollView() {
		DispatchQueue.main.async {
			var height:CGFloat = 0.0
			height += self.viewHeader.bounds.height
			
			let width = UIScreen.main.bounds.width
			switch self.currentCategory {
				case .feed:
					let a = ((width / 2) * 16/9) + 2
					height += (CGFloat(self.user.feeds.count) / 2).rounded(.up) * a
				case .dressing:
					height += CGFloat(ceil(Double(self.user.articles.count / 2))) * (width)
			}
			
			height -= 80
			
			self.heightScrollView.constant = height
		}
	}
	
	// MARK: Setup social media
	
	func setupSocialMedial() {
		self.stackViewSocialMedia.isHidden = self.user.networks.count == 0 ? true : false
		
		self.btnInsta.isHidden = self.user.networks.first(where: { return $0.type == .instagram }) == nil ? true : false
		self.btnTwitter.isHidden = self.user.networks.first(where: { return $0.type == .twitter }) == nil ? true : false
		self.btnFacebook.isHidden = self.user.networks.first(where: { return $0.type == .facebook }) == nil ? true : false
		self.btnSnapchat.isHidden = self.user.networks.first(where: { return $0.type == .snapchat }) == nil ? true : false
	}
	
}
