//
//  Ads.swift
//  Picdies
//
//  Created by Thomas Gallier on 26/02/2021.
//

import Foundation
import UIKit
import GoogleMobileAds

class Ads:NSObject,GADAdLoaderDelegate, GADUnifiedNativeAdLoaderDelegate {
	
	static let shared = Ads()
	private override init() {
		super.init()
	}
	
	//	let adUnitID = "ca-app-pub-9930442240386950/4311021159"
	let adUnitID = "ca-app-pub-3940256099942544/3986624511"
	var adLoader: GADAdLoader!
	var nativeAds = [GADUnifiedNativeAd]()
	
	
	func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
		print("NATIVE AD - didFailToReceiveAdWithError: \(error)")
	}
	
	func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
		print("NATIVE AD - didReceive: \(nativeAd)")
		self.nativeAds.append(nativeAd)
	}
	
	func requestAds(delegate: UIViewController) {
		
		let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
		multipleAdsOptions.numberOfAds = 5
		
		let imageOptions = GADNativeAdImageAdLoaderOptions()
		imageOptions.disableImageLoading = false
		
		let mediaOptions = GADNativeAdMediaAdLoaderOptions()
		mediaOptions.mediaAspectRatio = .square
		
		DispatchQueue.main.async {
			self.adLoader = GADAdLoader(
				adUnitID: self.adUnitID,
				rootViewController: delegate,
				adTypes: [.unifiedNative],
				options: [multipleAdsOptions, imageOptions, mediaOptions])
			self.adLoader.delegate = self
			self.adLoader.load(GADRequest())
		}
	}

}
	
	

