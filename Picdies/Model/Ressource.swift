//
//  Ressource.swift
//  Picdies
//
//  Created by Thomas Gallier on 22/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation

class Ressource {
	
	enum keys: String {
		case url, type
	}
	
	var url: URL?
	var type: Feed.feedType?
	
	required init(dict: [String: Any]) {
		if let s = dict[keys.url.rawValue] as? String { self.url = URL(string: s) }
		if let s = dict[keys.type.rawValue] as? String { self.type = Feed.feedType.init(rawValue: s) }
	}
	
}
