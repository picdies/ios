//
//  Tag.swift
//  Picdies
//
//  Created by Thomas Gallier on 22/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation

class Tag:BaseObject  {
	
	enum keys:String { case feeds }
	
	var feeds = [String]()
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.feeds.rawValue] as? [String] {
			s.forEach { (id) in
				self.addFeed(feed: id)
			}
		}
	}
	
	func addFeed(feed : String) {
		self.feeds.append(feed)
	}
	
	
}
