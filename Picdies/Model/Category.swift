//
//  Category.swift
//  Picdies
//
//  Created by Thomas Gallier on 19/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation

class Category:BaseObject  {
	
	enum keys:String {
		case articles
	}
	
	var articles = [Article]()
	
	required init(dict: [String: Any]) {
		super.init(dict: dict)
	}
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.articles.rawValue] as? [[String:Any]] {
			s.forEach { (dict) in
				self.addArticle(dict: dict)
			}
		}
	}
	
	func addArticle(dict : [String:Any]) {
		self.articles.append(Article(dict: dict))
	}
}
