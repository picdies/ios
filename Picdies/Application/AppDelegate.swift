//
//  AppDelegate.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import SwiftEntryKit
import FirebaseCore
import FBSDKCoreKit
import GoogleSignIn
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
	
	var window: UIWindow?
	var manager: GlobalManager?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		UITabBar.appearance().clipsToBounds = true
		UITabBar.appearance().shadowImage = nil
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Poppins-Light", size: 10)!], for: .normal)
		UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Poppins-Light", size: 10)!], for: .selected)
		
		FirebaseApp.configure()
		IQKeyboardManager.shared.enable = true
		IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
		
		GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
		GIDSignIn.sharedInstance().delegate = self
		
		GADMobileAds.sharedInstance().start(completionHandler: nil)
		GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["5162eb59a4bdcf7aaf10823dd5e25187"]
		
		self.manager = GlobalManager.shared
		
		ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
		
		return true
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		UserStorage.newUser = [:]
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
		
		// Handle facebook
		if ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation]) { return true }
		
		// Handle Google
		if GIDSignIn.sharedInstance()?.handle(url) ?? false { return true }
		
		return false
	}
	
	
	// MARK: Google
	
	func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
		if let error = error {
			print("Google sign in Error: \(error)")
			AccountManager.shared.handleGoogleLogin(nil)
			return
		}
		guard let authentication = user.authentication else {
			print("Google sign in Error: No auth found")
			AccountManager.shared.handleGoogleLogin(nil)
			return
		}
		let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
		AccountManager.shared.handleGoogleLogin(credential)
	}
	
	func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {}
	
	
}
