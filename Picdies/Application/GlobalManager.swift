//
//  ConnectionManager.swift
//  Picdies
//
//  Created by Thomas Gallier on 24/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import SwiftEntryKit

class GlobalManager:NSObject {

	static let shared = GlobalManager()
	private override init() {
		super.init()
		self.thread()
	}

	func applicationGoesOffline() -> (UIView, EKAttributes) {
		var attributes = EKAttributes.topToast
		attributes.name = "popup_offline"
		attributes.windowLevel = .alerts
		attributes.displayDuration = .infinity
		attributes.statusBar = .currentStatusBar
		attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width), height: .intrinsic)
		attributes.scroll = .disabled
		attributes.entryInteraction = .absorbTouches
		attributes.roundCorners = .all(radius: 8)
		attributes.entryBackground = .color(color: .standardBackground)
		attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.2), scale: .init(from: 1, to: 0.7, duration: 0.7)))
		attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
		let title = EKProperty.LabelContent(text: "Oops, votre connexion est indisponible", style: .init(font: UIFont(name: "Poppins-Regular", size: 16)!, color: .standardContent))
		let description = EKProperty.LabelContent(text: "Vérifié vos données ou votre connexion wifi.", style: .init(font: UIFont(name: "Roboto-Regular", size: 14)!, color: .standardContent))
		let image = EKProperty.ImageContent(image: (UIImage(named: "no_connection")?.withTintColor(.black))!, size: CGSize(width: 50, height: 50))
		let simpleMessage = EKSimpleMessage(image : image, title: title, description: description)
		let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)

		return (EKNotificationMessageView(with: notificationMessage), attributes)
	}



	func thread() {
		NetworkManager.shared.reachability.whenUnreachable = { reachability in
			if !SwiftEntryKit.isCurrentlyDisplaying(entryNamed: "popup_offline") {
				let d = self.applicationGoesOffline()
				SwiftEntryKit.display(entry: d.0, using: d.1)
			}

			NetworkManager.shared.reachability.whenReachable = { reachability in
				if SwiftEntryKit.isCurrentlyDisplaying(entryNamed: "popup_offline") {
					SwiftEntryKit.dismiss()
					APIFetcher.shared.cacheRequest.forEach { (request) in
						APIFetcher.shared.request(request.0, delegate: request.1, completion: { response in
							request.1.returnCacheRequest(completion: response)
						})
					}
				}
			}
		}
	}

	func error(error : Error) {
		if !SwiftEntryKit.isCurrentlyDisplaying(entryNamed: "popup_error") {
			var attributes = EKAttributes.topToast
			attributes.statusBar = .hidden
			attributes.name = "popup_error"
			attributes.windowLevel = .alerts
			attributes.displayDuration = 3
			attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width), height: .intrinsic)
			attributes.scroll = .disabled
			attributes.entryInteraction = .absorbTouches
			attributes.entryBackground = .color(color: .standardContent)
			attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.2), scale: .init(from: 1, to: 0.7, duration: 0.7)))
			let title = EKProperty.LabelContent(text: error.localizedDescription, style: .init(font: UIFont(name: "Poppins-Bold", size: 22)!, color: .standardBackground))
			let description = EKProperty.LabelContent(text: "", style: .init(font: UIFont(name: "Poppins-Bold", size: 22)!, color: .clear))
			let simpleMessage = EKSimpleMessage(title: title, description: description)
			let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
			SwiftEntryKit.display(entry: EKNotificationMessageView(with: notificationMessage), using: attributes)
		}
	}
}
