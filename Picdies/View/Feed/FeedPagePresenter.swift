//
//  FeedPagePresenter.swift
//  Picdies
//
//  Created by on Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import AVFoundation
import ProgressHUD

protocol FeedPagePresenterProtocol: class {
    func viewDidLoad()
    func fetchNextFeed() -> IndexedFeed?
    func fetchPreviousFeed() -> IndexedFeed?
    func updateFeedIndex(fromIndex index: Int)
}

class FeedPagePresenter: FeedPagePresenterProtocol, APIFetcherDelegate {

	func returnCacheRequest(completion: Error? ) {
		self.responseFetchFeeds(response: completion)
	}

    fileprivate unowned var view: FeedPageView
    fileprivate var feeds: [Feed] = []
    fileprivate var currentFeedIndex = 0

    init(view: FeedPageView) {
        self.view = view
    }
    
    func viewDidLoad() {
        configureAudioSession()
		self.feeds = APIFetcher.shared.allCached()
        fetchFeeds()
    }
    
    func fetchNextFeed() -> IndexedFeed? {
        return getFeed(atIndex: currentFeedIndex + 1)
    }
    
    func fetchPreviousFeed() -> IndexedFeed? {
        return getFeed(atIndex: currentFeedIndex - 1)
    }
    
    func updateFeedIndex(fromIndex index: Int) {
        currentFeedIndex = index
    }
    
    fileprivate func configureAudioSession() {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .moviePlayback, options: [])
    }
    
    fileprivate func fetchFeeds() {
		self.view.view.activityIndicator(show: self.feeds.count == 0)
		APIFetcher.shared.request(.feeds, delegate: self) { (response) in
			self.responseFetchFeeds(response: response)
		}
    }

	func responseFetchFeeds(response : Error?) {
		if let _ = response {
			return
		} else {
			self.feeds = APIFetcher.shared.allCached()
			self.view.presentInitialFeed(self.feeds.first ?? Feed(dict: [:]))
			self.view.view.activityIndicator(show: self.feeds.count == 0)
		}
	}
    
    fileprivate func getFeed(atIndex index: Int) -> IndexedFeed? {
        guard index >= 0 && index < feeds.count else {
            return nil
        }
        return (feed: feeds[index], index: index)
    }
}
