//
//  FeedCollectionBigCell.swift
//  Picdies
//
//  Created by Thomas Gallier on 12/01/2021.
//  Copyright © 2021 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol FeedCollectionBigCellProtocol {
	func tapLike(cell : FeedCollectionBigCell)
}

class FeedCollectionBigCell:UICollectionViewCell {
	
	@IBOutlet weak var like_number: UILabel!
	@IBOutlet weak var like_button: UIButton!
	@IBOutlet weak var imageFeed: UIImageView!

	var feed:Feed?
	var delegate:FeedCollectionBigCellProtocol?

	override func prepareForReuse() {
		super.prepareForReuse()
	}

	func configure(delegate: FeedCollectionBigCellProtocol, feed : Feed) {
		self.feed = feed
		self.delegate = delegate

		if feed.type == .picture {
			self.imageFeed?.sd_imageIndicator = SDWebImageActivityIndicator.medium
			self.imageFeed?.sd_setImage(with: feed.ressources.first?.url, placeholderImage: UIImage(named: "loading"))
		} else {
			self.imageFeed.sd_imageIndicator = SDWebImageActivityIndicator.medium
			self.imageFeed?.sd_setImage(with: feed.ressources.last?.url)
		}

		if UserStorage.likedFeed.contains(feed.id) {
			self.like_button.setImage(UIImage(named: "like/like-fill"), for: .normal)
			self.like_button.tintColor = .picdiesRed
		} else {
			self.like_button.setImage(UIImage(named: "like/like"), for: .normal)
			self.like_button.tintColor = .white
		}
		
		self.like_number.text = String(self.feed?.interaction?.like ?? 0)
		self.like_number.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapLike)))

	}

	@IBAction func didTapLike() {
		self.delegate?.tapLike(cell: self)
	}

}
