//
//  OverlayPlayer.swift
//  Picdies
//
//  Created by Thomas Gallier on 21/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import AVKit
import SwiftDate

protocol OverlayPlayerDelegate {
	func tapDismiss()
	func tapArticle(article : Article)
	func tapLike()
	func tapComment()
	func tapShare()
	func tapShowArticle()
	func tapUser()
}

class OverlayPlayer : UIView, UIScrollViewDelegate, ArticleFeedDelegate {

	// MARK: UI Fonctionnal

	@IBOutlet weak var heightOfStackViewTags: NSLayoutConstraint!
	@IBOutlet weak var opacifier: UIView!
	@IBOutlet weak var heightOfPseudo: NSLayoutConstraint!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var heightScrollView: NSLayoutConstraint!
	@IBOutlet weak var viewBottom: UIView!
	@IBOutlet weak var stackView: UIStackView!
	@IBOutlet weak var scrollViewContent: UIScrollView!

	@IBOutlet weak var informationMarginBottom: NSLayoutConstraint!
	// MARK: UI Button

	@IBOutlet weak var likeBtn: UIButton!
	@IBOutlet weak var commentBtn: UIButton!
	@IBOutlet weak var shareBtn: UIButton!


	// MARK: UI Informations

	@IBOutlet weak var leftViewSwipeImage: UIView!
	@IBOutlet weak var rightViewSwipeImage: UIView!
	@IBOutlet weak var backBtn: UIButton!
	@IBOutlet weak var thumbnailImage: UIImageView!
	@IBOutlet weak var stackViewArticle: UIStackView!
	@IBOutlet weak var interaction_like: UILabel!
	@IBOutlet weak var interaction_comment: UILabel!
	@IBOutlet weak var interaction_share: UILabel!
	@IBOutlet weak var user_name: UILabel!
	@IBOutlet weak var user_avatar: UIImageView!
	@IBOutlet weak var user_follow: UILabel!
	@IBOutlet weak var feed_name: UILabel!
	@IBOutlet weak var stackView_tags: UIStackView!

	var overlayPlayerDelegate:OverlayPlayerDelegate?
	var viewDelegate:UIView?
	var feed:Feed?
	var isPaging:Bool?

	var heightOfBottomView:Int {
		get {
			var height = 24
			if self.feed?.articles.count == 0 { self.stackViewArticle?.removeFromSuperview() }
			if self.feed?.tags.count == 0 { self.stackView_tags?.removeFromSuperview() }
			if self.feed?.name == nil { self.feed_name?.removeFromSuperview() }
			height += Int((self.feed?.articles.count == 0 ? -8 : self.scrollView?.bounds.height) ?? 0)
			height += Int((self.feed?.tags.count == 0 ? 0 : self.stackView_tags?.bounds.height) ?? 0)
			height += Int((self.feed?.name?.count == 0 ? 0 : self.feed_name?.bounds.height) ?? 0)
			return height
		}
	}
	
	func setupUI(feed : Feed, view : UIView, delegate:OverlayPlayerDelegate, isPaging:Bool) {
		self.viewDelegate = view
		self.overlayPlayerDelegate = delegate
		self.feed = feed
		self.isPaging = isPaging
		self.scrollViewContent.delegate = self
		self.frame = view.frame
		self.setupData()

		self.setupContent()
		self.setupGesture()
		self.setupStackViewTags()
		if self.feed?.type == .picture {
			self.createPagination(actualPage: self.scrollView.currentPage)
		} else {
			self.thumbnailImage.sd_imageIndicator = SDWebImageActivityIndicator.medium
			self.thumbnailImage?.sd_setImage(with: self.feed?.ressources.last?.url)
		}

	}

	
	func setupGesture() {
		self.interaction_like.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapLike(_:))))
		self.interaction_comment.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapComment(_:))))
		self.interaction_share.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapShare(_:))))
		self.user_avatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapUser)))
		self.user_name.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapUser)))
		self.user_follow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapUser)))
	}

	func setupContent() {
		DispatchQueue.main.async {
			switch self.feed?.type {
				case .picture:
					self.feed?.ressources.forEach({ (ressource) in
						self.createImageView(ressource: ressource)
					})
				default: break
			}
		}
	}

	func createImageView(ressource : Ressource) {
		let imageView = UIImageView(frame:UIScreen.main.bounds)
		imageView.translatesAutoresizingMaskIntoConstraints = false

		let imageViewWidthConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width)
		imageView.addConstraints([imageViewWidthConstraint])
		imageView.contentMode = .scaleAspectFill
		imageView.clipsToBounds = true
		imageView.layer.masksToBounds = true
		imageView.isUserInteractionEnabled = true
		DispatchQueue.main.async {
			imageView.sd_setImage(with: (ressource.url)! as URL, completed: {_,_,_,_ in })
		}
		self.stackView.addArrangedSubview(imageView)
	}

	func setupData() {
		self.user_name.text = self.feed?.user?.name
		DispatchQueue.main.async {
			if let url = self.feed?.user?.avatar {
				self.user_avatar.sd_setImage(with: url) { image ,_,_,_ in
					if image == nil {
						self.user_avatar.image = UIImage(named: "loading")
					}
				}
			} else {
				self.user_avatar.image = self.feed?.user?.getAvatar()
			}
		}
		
		self.user_follow.text = feed?.stringDate
		self.user_avatar.border(width: 1, color: .picdiesRed, radius: nil)

		self.feed_name.text = self.feed?.name
		self.interaction_share.text = String(self.feed?.interaction?.share ?? 0)
		self.interaction_like.text = String(self.feed?.interaction?.like ?? 0)
		self.interaction_comment.text = String(self.feed?.interaction?.comment?.count ?? 0)


		if UserStorage.likedFeed.contains(feed?.id ?? "") {
			self.likeBtn.setImage(UIImage(named: "like/like-fill"), for: .normal)
			self.likeBtn.tintColor = .picdiesRed
		} else {
			self.likeBtn.setImage(UIImage(named: "like/like"), for: .normal)
			self.likeBtn.tintColor = .white
		}


		self.backBtn.isHidden = self.isPaging ?? false
	}

	func setupStackViewArticle() {
		self.stackViewArticle?.arrangedSubviews.forEach { (view) in
			if view.tag != 99 {
				view.removeFromSuperview()
			}
		}
		var index = 1
		for article in self.feed?.articles ?? [] {
			let v = ArticleFeed.makeFromNib()
			v.setupUI(delegate: self)
			v.setupData(article: article)
			v.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width - 8, height: 100)
			v.layoutIfNeeded()
			let width = NSLayoutConstraint(item: v, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: self.scrollView.frame.width - 8)
			v.addConstraint(width)
			self.stackViewArticle?.insertArrangedSubview(v, at: index)
			index += 1
		}
	}

	func setupStackViewTags() {
		self.stackView_tags?.arrangedSubviews.forEach { (view) in
			view.removeFromSuperview()
		}
		self.feed?.tags.forEach({ (tag) in
			self.stackView_tags.addArrangedSubview(self.createTagView(tag: tag))
		})
		self.heightOfStackViewTags?.constant = 20
	}

	func createTagView(tag : Tag) -> UILabel {
		let labelTag = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 14))
		labelTag.text = "#" + (tag.name ?? "")
		labelTag.textColor = .white
		labelTag.font = UIFont(name: "Poppins-SemiBold", size: 11)
		labelTag.sizeToFit()
		let widthConstraint = NSLayoutConstraint(item: labelTag, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: labelTag.frame.width)
		labelTag.addConstraints([widthConstraint])
		return labelTag
	}

	@IBAction func tapLike(_ sender: Any) {
		self.overlayPlayerDelegate?.tapLike()
	}
	@IBAction func tapComment(_ sender: Any) {
		self.overlayPlayerDelegate?.tapComment()
	}
	@IBAction func tapShare(_ sender: Any) {
		self.overlayPlayerDelegate?.tapShare()
	}

	@IBAction func dismiss(_ sender: Any) {
		self.overlayPlayerDelegate?.tapDismiss()
	}

	@objc func tapUser() {
		self.overlayPlayerDelegate?.tapUser()
	}

	func tapArticle(article: Article) {
		self.overlayPlayerDelegate?.tapArticle(article: article)
	}
	


	// MARK: Pagination

	func createPagination(actualPage: Int) {
		UIView.animate(withDuration: 0.2, animations: {
			let numberOfImage = self.feed?.ressources.count ?? 0
			if actualPage == numberOfImage - 1 { self.rightViewSwipeImage.backgroundColor = .clear }
			else { self.rightViewSwipeImage.backgroundColor = .white }
			if actualPage == 0 { self.leftViewSwipeImage.backgroundColor = .clear }
			else { self.leftViewSwipeImage.backgroundColor = .white }
		})
	}

	// MARK: Scroll

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let currentPage = scrollView.currentPage
		self.createPagination(actualPage: currentPage)
	}
}

extension UIScrollView {
   var currentPage: Int {
	  return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)
   }
}
