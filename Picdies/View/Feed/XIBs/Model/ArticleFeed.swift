//
//  ArticleFeedCell.swift
//  Picdies
//
//  Created by Thomas Gallier on 29/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol ArticleFeedDelegate {
	func tapArticle(article : Article)
}
class ArticleFeed:UIView {

	@IBOutlet weak var arrow_right: UIButton!
	@IBOutlet weak var likeButton: UIButton!
	@IBOutlet weak var viewBookmark: UIView!
	@IBOutlet weak var nameArticle: UILabel!
	@IBOutlet weak var priceArticle: UILabel!
	@IBOutlet weak var brandArticle: UILabel!
	@IBOutlet weak var imageArticle: UIImageView!

	var delegate:ArticleFeedDelegate?
	var article:Article?
	
	func setupUI(delegate : ArticleFeedDelegate) {
		DispatchQueue.main.async {
			self.layer.masksToBounds = true
			self.roundCorners(.allCorners, radius: 16)
			self.arrow_right.layer.cornerRadius = self.arrow_right.frame.height / 2
			self.arrow_right.layer.borderWidth = 1
			self.imageArticle.roundCorners(.allCorners, radius: 16)
		}
		self.delegate = delegate
		self.viewBookmark.backgroundColor = UIColor.white
		self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapArticle)))
	}

	@objc func didTapArticle() {
		self.delegate?.tapArticle(article: self.article!)
	}

	func setupData(article : Article) {
		self.article = article
		self.imageArticle.sd_imageIndicator = SDWebImageActivityIndicator.medium
		self.imageArticle.sd_setImage(with: article.ressources.first?.url) {image,_,_,_ in
			if image != nil {
				self.arrow_right.isHidden = false
				self.likeButton.isHidden = false
				
				self.brandArticle.text = article.brand
				self.nameArticle.text = article.name
				self.priceArticle.text = article.price != nil ? (article.price ?? "") + " €" : ""
			}
		}
	}
}
