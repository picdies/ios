//
//  PublicityCollectionBigCell.swift
//  Picdies
//
//  Created by Thomas Gallier on 09/02/2021.
//

import Foundation
import UIKit
import SDWebImage
import GoogleMobileAds


extension PublicityCollectionBigCell : GADVideoControllerDelegate {
	  func videoControllerDidPlayVideo(_ videoController: GADVideoController) {
	  }

	  func videoControllerDidPauseVideo(_ videoController: GADVideoController) {

	  }

	  func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {

	  }
}

class PublicityCollectionBigCell:UICollectionViewCell {

	@IBOutlet weak var labelSponso: UILabel!
	@IBOutlet weak var nativeAdView: GADUnifiedNativeAdView!
	@IBOutlet weak var mediaView: GADMediaView!
	@IBOutlet weak var headline: UILabel!
	@IBOutlet weak var avatarImage: UIImageView!

	

	override func prepareForReuse() {
		super.prepareForReuse()
		nativeAdView.nativeAd = nil
		mediaView?.mediaContent = nil
		mediaView?.activityIndicator(show: false)
		headline.text = ""
		avatarImage.image = nil
		avatarImage.isHidden = true
		labelSponso.text = ""
	}

	func configure(nativeAd: GADUnifiedNativeAd) {
		if nativeAd.mediaContent.hasVideoContent {
			mediaView?.activityIndicator(show: true)
		}
		mediaView?.mediaContent = nativeAd.mediaContent
		mediaView?.contentMode = .scaleAspectFill

		nativeAdView.mediaView = mediaView

		headline.text = nativeAd.headline
		avatarImage.image = nativeAd.icon?.image ?? #imageLiteral(resourceName: "loading")

		nativeAdView.isUserInteractionEnabled = false

		nativeAd.mediaContent.videoController.delegate = self

		nativeAdView.nativeAd = nativeAd

		labelSponso.text = "Sponsorisé"
		avatarImage.isHidden = false
	}


}
