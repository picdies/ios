//
//  ArticleCollectionMediumCell.swift
//  Picdies
//
//  Created by Thomas Gallier on 09/02/2021.
//

import Foundation
import UIKit
import SDWebImage

protocol ArticleCollectionMediumProtocol {
	func tapBookmark(cell : ArticleCollectionMediumCell)
}

class ArticleCollectionMediumCell:UICollectionViewCell {


	var article:Article?
	var delegate:ArticleCollectionMediumProtocol?

	override func prepareForReuse() {
		super.prepareForReuse()
	}

	func configure(delegate: ArticleCollectionMediumProtocol, article : Article) {
		self.article = article
		self.delegate = delegate
	}

	@IBAction func didTapBookmark() {
		self.delegate?.tapBookmark(cell: self)
	}
}
